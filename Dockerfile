FROM debian:10 as build

RUN apt-get update && \
    apt-get install git -y && \
    git clone https://gitlab.com/damien-citaire/adrec-notes.git

WORKDIR /adrec-notes

FROM nginx:latest

COPY --from=build /adrec-notes/docs /usr/share/nginx/html

## Commandes : 
# docker build -t adrec-notes . --no-cache
# docker run -it --rm -d -p 8080:80 --name adrec_notes adrec-notes

## Bequille :
# https://www.docker.com/blog/how-to-use-the-official-nginx-docker-image/