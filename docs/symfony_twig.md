# TWIG

**Moteur de template**, language spécial, propre **pour les vues**. Adapté pour les non-développeurs *(les designers quoi)*.

## Syntaxe

### Commentaire twig

```html
{# Je suis un commentaire en twig #}
```

<br>

### Tag twig, la logique du code

```html
{%  if num > 5 %}
    <p>Supérieur à 5</p>
{%  endif %}
```

<br>

### Variable twig

```html
{{ var_msg }}
```

<br>
<br>


```html
<li class="nom-{{ nom_classe }}">blbl</li>
```
?> On peux aussi en mettre dans des attribut de balise html


### Filtre

On ajoute un **pipe** ( `|` ) dans notre tag pour appliquer des filtres (un `length`, un `formatage de date`, un `replace`, un `default` ...).


**Exemples:**

```php
Compteur : {{ compteur|default("Il n'y a pas de compteur") }}
```

?> le `default` sert à afficher une valeur par defaut si jamais notre variable est `null` ou pas renseignée par exemple.<br>
Ici, si on n'a pas de compteur, on affiche `"Il n'y a pas de compteur"`

<br>

```php
Date du jour : {{ date|date('d/m/Y') }}
```
?> On affiche la date du jour au format `'d/m/Y'`

<br>

**Un peu plus Costaud :**

```php
{% if boissons|length > 0 %}
    <ul>
        {% for key, boisson in boissons %}
            <p>{{ loop.index }}</p>
            {% if loop.first %}
                <div style="color: red;">JE SUIS LE PREMIER</div>
            {% endif %}
            <li>
                {{ boisson }}
            </li>
        {% endfor %}
    </ul>
{% else %}
    <p>Le bar est vide, on est fermé</p>
{% endif %}
```

?> Ici, dans un array boisson, s'il y a **au moins un resultat**, on fait un `for` pour afficher le **nom de chaques boissons** ainsi que son **index** (avec `loop.index`).<br>
Si on est sur le **1er résultat de la boucle**, on affiche une div avec un `style="color:red"` qui dit que c'est le premier resultat (on fait ça grace à `loop.first`).<br>
Si on n'a pas de boisson à afficher, on affiche un message à la place.

#### Loop dans twig

Quand on fait une **boucle for** dans **twig**, on peux générer un index incrémental via `loop`.

Exemple : 

```html
{% for post in 0..2 %}
    <div class="col-lg-4 mb-5">
        {% include 'Front/Partials/card/default-card.html.twig' with {'index': loop.index} %}
    </div>
{% endfor %}
```

#### Pour aller + loin dans les filtres :
https://twig.symfony.com/doc/3.x/filters/index.html

### Paths

```html
<a href="{{ path('ma_route', {'id': 5}) }}">
    Mon lien
</a>
```

?> Permets de créer des liens entre les routes. Le path correspond au name de la Route (passé dans le controller).


Si on veux definir une route qui prends un paramètre, on adopte cette syntaxe : 

**Controller :** 

```php
/**
* @Route(path="/boisson/show{toto}", name="boisson_show")
*/
```

**Vue :**

```html
<a href="{{ path('boisson_show', {'toto': boisson}) }}">
    Mon lien
</a>
```
