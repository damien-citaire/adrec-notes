## Modifications dans une table

Pour **modifier des données** dans les tables on peux être ammené à taper une des commandes suivantes :

## Les commandes

### Mise à jour *UPDATE*

Si on veux mettre à jour un champs.

```sql
UPDATE voiture
SET prix = 6200
WHERE id = 2
```
?> On modifie le prix de la voiture avec l' **ID 2** pour lui coller un prix à **6200€**

<br>


```sql
UPDATE voiture
SET couleur = 'Bleu canard'
WHERE id = 5
```

?> On modifie la couleur de la voiture avec l' **ID 5** pour lui coller une couleur **bleu canard**

<br>

**Exemple plus avancé combiné avec des `SELECT` embarqués:**

```sql
UPDATE student

SET height = 167,
    sexe = 'M',
    first_name = (SELECT first_name FROM contacts WHERE id = 5),
    last_name = (SELECT last_name FROM contacts WHERE id = 5)

WHERE id = 63
```