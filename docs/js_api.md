# API

## Requête Javascript sur une API

**Asynchrone :**<br>
Requête déclenchées par le javascript, elle sont possibles alors que la page est chargée **sans devoir reload** :muscle:<br>
Un requête synchrone rechargera la page, tandis qu'en **Asynchrone** ce n'est pas le cas. :muscle: :ok_hand: 

**API :**<br>
**A**pplication **P**rogramming **I**nterface.<br>
Permets de communiquer avec un service en ligne

## Tester les API

*jsonplaceholder fournis des infos de remplissage pour l'exemple*

```js
// On fetch l'url de l'api que l'on vise :
fetch('https://jsonplaceholder.typicode.com/todos')
    .then(
        // On appelle la fonction anonyme 
        // qui ne sera executé que quand on a la réponse
        // du serveur où on tape :
        (response) => {
            console.log(response);
        }
    );
```

?> On `fetch` l'url de l'api que l'on vise (*https://jsonplaceholder.typicode.com/todos*), via le `then`, on **attends** la `response` de ce service et on le `console.log()`.

> Le `then()` fait que ce qu'il contient ne sera executé que quand la condition au dessus sera executée.

## Ajouter une valeur en local à une requête

```js
fetch('https://jsonplaceholder.typicode.com/todos')
    .then(
        (response) => {
            return response.json();
        }
    )
    .then((foo) => {
        console.log(foo.length);
        foo.unshift('foobar')
        console.log(foo);
    });
```

?> On `fetch` l'url de l'api puis on attends sa reponse qu'on récupère via le `return`.<br><br>
Ensuite, sous le nom `foo`, on affiche via le `.length` le nombre d'entrées qu'il y a dans l'objet `foo` *(qui est, je le rapelle le resultat du `return` fais juste au dessus)*.<br><br>
On enchaîne en ajoutant la valeur `foobar` à `foo` avec le `.unshift`.<br><br>
Enfin, on fait le `console.log()` de `foo`.

Si on veux on peux aussi parcourir le response comme un tableau on peu faire un `for`si on veut  : 

```js
fetch('https://jsonplaceholder.typicode.com/todos')
    .then(
        (response) => {
            return response.json();
        }
    )
    .then(
        (foo) => {
            for (let item of foo) {
                console.log(item.title);
            }
        }
    );
```


## Créer une fonction pour récupérer la liste des utilisateurs

```js
function getUsers() {

    fetch('https://jsonplaceholder.typicode.com/users')
        .then(
            (response) => {
                return response.json();
            }
        )
        .then((foo) => {
            console.log(foo);
        });

};

console.log(getUsers());
```

?> On `fetch` l'url de l'api puis on attends sa reponse qu'on récupère via le `return`.<br><br>
On affiche son contenu avec les `console.log()`.


## Créer une fonction pour récupérer un utilisateur précis en fonction de son ID

```js
function getSpecificUser(index = 0) {

    fetch('https://jsonplaceholder.typicode.com/users')
        .then(
            (response) => {
                return response.json();
            }
        )
        .then((foo) => {
            console.log(foo[index]);
        });
    
};
    
console.log(getSpecificUser(4));
```

?> On `fetch` l'url de l'api puis on attends sa reponse qu'on récupère via le `return`.<br><br>
Ensuite, sous le nom `foo`, on affiche juste l'entrée dont l'index est passé en paramètre de la fonction *(ici ce sera le 5eme utilisateur (index 4 mais on part de 0)*


## Créer une fonction pour ajouter un nouvel utilisateur

*La fonction prend en arguments le nom et le prénom de la personne*

```js
function addUser(firstName, lastName) {

    let fullName = firstName.concat(' ', lastName);

    const data = {
        name: fullName,
    };

    fetch('https://jsonplaceholder.typicode.com/users', {
        method: 'POST',
        body: JSON.stringify(data),
    })

        .then((response) => {
            return response.json();
        })
        .then((remoteData) => {
            console.log(remoteData);
        });
    
};
    
console.log(addUser('John', 'Smith'));
```

?> On recupère nos 2 arguments et on les concatène sous la variable `fullName`, <br><br>
On stocke ensuite dans la variable `data` notre `fullName` en l'associant à la clé `name` *(qui existe dans ce qu'on a en retour au `fetch` qui sera fait ensuite)*<br><br>
On fait notre `fetch`, en ajoutant la `method: POST` ainsi que le `body: JSON.stringify(data)`.<br>
Le `POST` fait qu'on **envoie** les infos au serveur, le `body` est **ce qui sera contenu** dans notre envoi. Le body contien notre `data`.<br><br>
On récupère la réponse<br><br>
Ensuite, sous le nom `remoteData`, on affiche le resultat de la requête *(ici ce sera juste le POST qu'on a fait*


> La majorité des API fonctionnent en Json.<br><br>
`JSON.stringify` : transforme du  JS en JSON. A utiliser pour préparer les données à mettre dans le `body`

**Autres exemples :**

```js

// Exemple 1

const data = {
    title: 'Foobar',
    coucou: 'Bonjour',
    hello: 'Salut',
};


fetch('https://jsonplaceholder.typicode.com/todos', {
    method: 'POST',
    body: JSON.stringify(data),
})
    .then((response) => {
        return response.json();
    })
    .then((remoteData) => {
        console.log(remoteData);
    });


// Exemple 2

fetch('https://jsonplaceholder.typicode.com/todos', {
    method: 'POST',
    body: JSON.stringify({
        title: 'Foobar',
        coucou: 'Bonjour',
        hello: 'Salut',
    }),
})
    .then(
        (response) => {
            console.log(response.status);
            return response.json();
        }
    )
    .then(
        (data) => {
            console.log(data);
        }
    );
```
