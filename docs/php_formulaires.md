# Les Formulaires

## Bases

`GET` Requete pour afficher des données<br>
`POST` Soumission de données *(on ne la vois pas)*

`var_dump($_GET);` et  `dump($_GET);` nous affichent le dump de ce qu'il y a dans la requête `GET`.

## Les vérifications

!> Avec les formulaires il faut bien s'assurer de pleins de choses, (si les var sont definis, s'il s'agit bien du type de valeur attendue...)

### Vérifications des variables

Si on appelle des fonctions avec des `$_GET`, il faut s'assurer que les variables neccessaires sont définies.

Pour s'en assurer, on fais un `if` avec des `isset`:

```php
if(isset($_GET['height']) && isset($_GET['weight'])) {

    $imc = getImc($_GET['height'], $_GET['weight']);
    dump($imc);

}
```
?> Admettons qu'on a une fonction de calul d'IMC qui prends en arguments 2 floats.<br>
Via le `if` ci-dessus, **on s'assure que `height` et `weight` sont bien définis.**

<br>

On peux faire une verification similaire avec `!empty` :

```php
if(!empty($_GET['height']) && !empty($_GET['weight'])) {

    $imc = getImc($_GET['height'], $_GET['weight']);
    dump($imc);

}
```

### Vérification des entiers

Si on veux s'assurer qu'on a bien un **entier**, on peux utiliser `intval` : 

```php
if(!empty($_GET['height']) && !empty($_GET['weight'])) {

    $h = intval($_GET['height']);
    $w = intval($_GET['weight']);

    $imc = getImc($h, $w);
    dump($imc);

}
```
?> On a ici le même code pour l'imc qu'en haut sauf qu'en plus on s'assure que height et weight sont bien des **entiers** en passant pas un check des vars `$h` et `$w`.


### Vérification des nombres à virgule

Si on veux s'assurer qu'on à bien un **nombre à virgules**, on peux utiliser `floatval` : 

```php
if(!empty($_GET['height']) && !empty($_GET['weight'])) {

    $h = floatval($_GET['height']);
    $w = floatval($_GET['weight']);

    $imc = getImc($h, $w);
    dump($imc);

}
```

## Exemple d'un formulaire <br> *(basique en $_GET)*

*(2 fichiers : index.php et function.php)*

Contenu du fichier **index.php**

```php
<?php
// le require_once permet de s'assurer que le fichier est importé une seule fois :
require_once 'functions.php';

// Le if avec le !empty permet de s'assurer que la fonction est appelée 
// seulement quand le(s) variable(s) est(sont) définie(s) :
if(!empty($_GET['height']) && !empty($_GET['weight'])) {
    $h = floatval($_GET['height']);
    $w = floatval($_GET['weight']);

    $imc = getImc($h, $w);
    echo 'Affichage de l\'IMC :';
    dump($imc);
}

// Le dump (fonction faîte pour afficher un var_dump de manière plus lisible)
// nous affiche le contenu du $_GET
echo 'Affichage du $_GET :';
dump($_GET);
?>

<!-- Notre html avec le formulaire : -->
<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Formulaires</title>
</head>

<body>
<h1>Calcul IMC</h1>
<form method="get">
    <div>
        <label for="height-input">Taille</label>
        <input
            id="height-input"
            type="number"
            name="height"
            value="170"
            min="0"
            step="0.5"
        >
    </div>
    <br>
    <div>
        <label for="weight-input">Poids</label>
        <input
            id="weight-input"
            type="number"
            name="weight"
            value="65"
            min="0"
            step="0.5"
        >
    </div>
    <br>
    <button type="submit">Calculer</button>
    <button><a href="/">Vider</a></button>
</form>
<br><br>

<!-- Le if ci-dessous affiche un message différent
si on a un imc ou pas encore-->
<?php if(!empty($imc)):?>
    <div>
        Mon IMC est de : <strong><?php echo $imc ?></strong>
    </div>
<?php else:?>
    <div>
        Mon IMC n'est pas encore calculé.
    </div>
<?php endif;?>

</body>
</html>
```

Contenu du fichier **function.php**

```php
<?php

/**
 * Custom dump from Theau
 * Better than var_dump
 * @param $var
 */
function dump($var): void
{
    echo '<pre>' . var_export($var, true). '</pre>';
}

/**
 * Get IMC from height and weight
 * @param float $height in cm
 * @param float $weight in kg
 * @return float
 */
function getImc(float $height, float $weight): float
{
    $height /= 100;
    return round($weight / ($height * $height), 2);
}
```

?> En retour on a donc ça sur notre page web : <br><br>
*Etape 1, le formulaire possède les valeur spar default, aucunimc n'est retourné et l'array du $_GET est vide*
![](img/php_formulaire-basic1.png)<br>
*Etape 2, le formulaire avec les valeur 180 et 63 a été soumis, le tableau du $_GET nousdonne les infos envoyé et la calcul de l'imc a été fait.*
![](img/php_formulaire-basic2.png)<br>


<br><br>


## L'authentification

### Exemple d'un Formulaire d'authentification

*(2 fichiers : index.php et function.php)*

> On notera qu'icic le method du formulaire est **post**

Le formulaire dans le fichier **index.php** : 

```php
<form
    method="post"
    action="secret.php"
>
    <input
        id="login"
        type="text"
        name="login"
        placeholder="Login"
    >
    <input
        id="password"
        type="password"
        name="passwd"
        placeholder="Mot de passe"
        autocomplete="off"
    >
    <button type="submit">Se connecter</button>
</form>
```

Le fichier **secret.php** appelé par le formulaire ci-dessus : 

```php
<?php

$login = 'toto';
$passwd = 'titi';

// On check si on a des données
if(!empty($_POST['login']) && !empty($_POST['passwd'])) {
    // Si on a des données, on regarde si le login et/ou le password sont faux
    if($_POST['login'] !== $login || $_POST['passwd'] !== $passwd) {
        header ('Location: /');
        die('Error redirect');
    }
} else {
    // Dans les 2 cas, si on n'est pas bons, on redirect vers la racine
    header ('Location: /');
    die('Error redirect');
}
?>

<!-- Si on passe les checks on affiche le html ci-dessous -->
<!doctype html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <title>Formulaires</title>
</head>

<body>
    <p>Bien joué, c'était les bons login/password</p>
</body>

</html>
```

### Les mots de passe

Par sécurité, on **hash** les mots de passe afin de ne pas les stocker en clair.<br>
*Il existe différentes méthodes de hashage de mots de passe.*

Comment on fait ?
Exemple avec la methode `password_hash`: 

```php
$hash = password_hash('titi', PASSWORD_DEFAULT));
```
?>On hash le password titi.

> Par sécurité, on peux hasher le mot de passe plusieurs fois, ca complexifie, et si jamais il y a plusieurs mots de passes identiques ils seront impossible à identifer car hashé à plusieurs reprises

Exemple :

```php
$hash = password_hash('titi', PASSWORD_DEFAULT); // 1er passage à la moulinette
$hash = password_hash('titi', PASSWORD_DEFAULT); // 2eme passage à la moulinette

var_dump($hash) // Affichage du résultat
```

?> Le `var_dump`du `$hash` de `titi` donne ceci : <br>
string(60) "**$2y$10$akUhzSfKX4R7w1IP5x6UOOVlNTpWyjOxL6J4OOC3yAzc..PbKWpVW**"<br>
*pas très digeste ...*

<br>

On peux le vérifier en faisant un `password_verify` :
 ```php
dump(password_verify('titi', $hash));
 ```
?> Renvoie un `bool` pour s'assurer que le mot de passe titi est bien hashé.


## Les sessions

L'intéret d'une session est de savoir quand une session est lancée et de la stocker dans un cookie.<br>
Ca evite d'avoir à se reloguer à chaque page accessible via login/pswd.<br>

Dans les fichier php il faut un `session_start();` avant chaque contenu html pour la verif de session en cours ou pas.

### Exemple d'un formulaire d'authentification avec création de session

Commençons par l'arborescence du projet, elle a cette forme : 
![](img/php_session-auth-arbo.png)<br>

Le fichier **function.php**

```php
<?php
function init(): void
{
    // Lance la session
    session_start();
}

function forceLogin(): void
{
    // Appelle la fonction init() pour lancer la session
    // s'assure que l'authentification est faite
    init();
    // sinon redirige vers la page de login
    if(!isset($_SESSION['logged_in'])) {
        header('Location: /login.php');
    }
}
```

Les fichiers **head.php** et **nav.php** contiennent des morceaux de page qui seront présent sur l'ensembles des page. <br> *Pourquoi s'embêter à le recopier alors qu'on peux les avoir en 1 seul exemplaire qu'on importe lorsqu'il y en a besoin* 

Le fichier **head.php**<br>
*Ne contient que du html avec le debut de chaque fichiera insi que son <head>*

```html
<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Login example</title>
    <style>
        body { padding: 16px; }
        label { display: none; }
        input { margin-bottom: 1rem; }
    </style>
</head>
```

Le fichier **nav.php**<br>
*Contient que des liens de navigation avec affichage conditionnel*

```php
<?php
    require_once 'functions.php';
    init();
?>

<?php if (isset($_SESSION['logged_in'])): ?>
    <a href="logout.php">Déconnexion</a>
<?php else: ?>
    <?php 
    // Bon ici le lien vers la connexion ne sert pas trop car le
    // fichier nav.php est seulement chargé dans une session ouverte :D
    ?>
    <a href="login.php">Connexion</a>
<?php endif; ?>
```

Le fichier **index.php** <br>
*Ne s'affiche que si on est loggé, sinon redirect vers `login.php` via la fonction `forceLogin()`*

```php
<?php
require_once 'functions.php';
// Appel de la fonction qui vérifie la connexion
forceLogin();
?>

<!doctype html>
<html lang="fr">
<!--Importation du début du chier html-->
<?php include 'src/partials/head.php' ?>

<body>
    <h1>Accueil</h1>
    <!--Importation de la navigation-->
    <?php include 'src/partials/nav.php' ?>
</body>
</html>
```

Le fichier **login.php** avec le formulaire de connection<br>
*S'affiche que si on n'est pas loggé*

```php
<?php
require_once 'functions.php';
init();

$loginError = false;

// le couple login/mot de passe attendu
$login = 'toto';
$passwd = 'titi';

// On check si on a des données
if(!empty($_POST['login']) && !empty($_POST['passwd'])) {
    if($_POST['login'] === $login && $_POST['passwd'] === $passwd) {
        $_SESSION['logged_in'] = true;
        header('Location: /');
    } else {
        $loginError = true;
    }
}
?>

<!doctype html>
<html lang="fr">
<?php include 'src/partials/head.php' ?>

<body>
<h2>Login</h2>
<p>Rentrez votre login/mot de passe pour accéder à l'accueil</p>
<form method="post">
    <div>
        <label for="login">Login</label>
        <input type="text" name="login" id="login" placeholder="Login">
    </div>
    <div>
        <label for="password">Mot de passe</label>
        <input id="password" type="password" name="passwd" placeholder="Mot de passe">
    </div>
    <div>
        <button type="submit">Se connecter</button>
    </div>
    <?php if($loginError):?>
        <div style="color: red; margin-top: 8px">Soucis avec login/pswd</div>
    <?php endif; ?>
</form>

</body>
</html>
```


Le fichier **logout.php**<br>
*Appelé via le lien de déconnection*

```php
<?php
session_start(); // Démarre la session
session_destroy(); // Supprime la session -> impossible sans session_start

header('Location: /'); // Redirige vers l'adresse en paramètre
?>
```

?> On a donc une redirection vers login quand on veut taper sur la page d'accueil et qu'on n'est pas loggé.<br><br>
La page login nous affiche un chouette formulaire à remplir :<br>
![](img/php_session-auth-form.png)
<br><br>
Une fois le formulaire rempli avec les bons identifiants/mot de passe on a enfin accès à l'accueil<br>
![](img/php_session-auth-logged.png)