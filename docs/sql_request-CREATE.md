## Création dans une table

Pour **inserer des données** dans les tables on peux être ammené à taper une des commandes suivantes :

## Les commandes

### Altération *ALTER*

Pour altérer une table (**la modifier**), on utilise la commande suivante :<br>

```sql
ALTER TABLE voiture ADD COLUMN prix NUMERIC
```

?> On ajoute une **colonne prix** de type numerique à la **table voiture**

<br>

### Insertion *INSERT INTO*

Pour **ajouter des champs** dans des tables, on utilise la commande suivante :<br>

```sql
INSERT INTO marque (`nom`, `pays_origine`) VALUES ('Renault', 'France');
```

?> On ajoute les **champs nom et pays_origine** dans la **table marque** avec les **valeurs Renault et France**

On peux aussi faire comme ça si on veux ajouter plus de données:

```sql
INSERT INTO marque (`nom`, `pays_origine`) VALUES
('Opel', 'Allemagne'),
('Renault', 'France'),
('Toyota', 'Japon');
```

<br>

### *INSERT INTO* avec *SELECT*

Pour ajouter des champs dans des tables **à partir d'autres tables**, on utilise la/les commande(s) suivante(s) :<br>

```sql
INSERT INTO students (`last_name`, `first_name`) VALUES
SELECT last_name, first_name FROM cours.students;
```

On peux aussi faire comme ça si on veux ajouter des données plus ciblées:

```sql
INSERT INTO students (`last_name`, `first_name`)
VALUES (
    (SELECT last_name, FROM contacts WHERE id = 36),
    (SELECT first_name FROM contacts WHERE in = 36)
);
```