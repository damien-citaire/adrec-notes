# Les commandes

Objet Command basique de Symfony, elle sont stockées dans le dossier `src/Command`.

Pour créer une command, on tape :

```bash
php bin/console make:command
```

## Exemple de commandes

Prenons un exemple avec un site de vente de voitures.<br>
On a, entre autres, ces entities qui concernent les voitures : 
- Brands,
- Categories,
- Model

Nos entités existent, ce qu'on veux c'est que quand quelqu'un recupère le projet il n'ai pas à renseigner les premières voitures à la main pour commencer à remplir la BDD. On va donc créer 3 Commands : 
- Une qui renseigne les Marques (Brands),
- Une qui renseigne les Catégories (Category),
- Une qui regroupe les command de **Brand**, puis de **Category**, puis qui **importe des modèles** de voitures (**Models**).

### La commande Brands

```bash
php bin/console make:command

Choose a command name (e.g. app:gentle-kangaroo):
> brands
```
?> Un fichier `BrandsCommand.php` est généré dans le dossier `src/Command`.

On peux déjà commencer par préparer notre command :

```php
// ++
use Doctrine\ORM\EntityManagerInterface;

// --
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

// ...

class BrandsCommand extends Command
{
    // -----------
    // On renseigne notre nom / description pour notre command :
    // -----------

    protected static $defaultName = 'brands';
    protected static $defaultDescription = 'Add brands to your Database';


    // -----------
    // On importe notre EntityManagerInterface (check le use + haut) :
    // -----------

    /**
     * @var EntityManagerInterface
     */
    protected EntityManagerInterface $em;


    // -----------
    // On créé un tableau avec nos brands à renseigner :
    // -----------

    /**
     * @var array
     */
    protected array $brands = [
        'Renault',
        'Opel',
        'Mercedes',
        'Toyota',
        'Nissan',
        'Ford',
        'Ferrari',
        'Volkswagen',
        'Audi',
        'WackyRaces',
        'CTR',
    ];


    // -----------
    // On s'occupe de créer le constructor de notre class
    // en y ajoutant notre EntityManagerInterface :
    // -----------

    /**
     * CreateBrandCommand constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        parent::__construct();
    }

    // -----------
    // On adapte le configure() généré par le maker
    // -----------
    protected function configure()
    {
        $this
            // On garde le description
            ->setDescription(self::$defaultDescription)
            // Par contre, ici on n'as pas besoin d'argument ou d'option,
            // on va les virer :
            // (on peux retirer les useless relatifs imports aussi)
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

// ...

```

Il va nous rester à gérer les actions que va faire la command quand elle s'executera, 

```php
// ++
use App\Entity\Brand;
use Symfony\Component\Console\Helper\ProgressBar;

// --
use Symfony\Component\Console\Style\SymfonyStyle;


// ...



/**
* @param InputInterface $input
* @param OutputInterface $output
* @return int|void
*/
protected function execute(InputInterface $input, OutputInterface $output)
{
    $output->writeln('<info>Creating all brands...');

    // -----------
    // On créé une progress bar en se basant sur la taille de notre tableau de brands :
    // - 1er argument -> $output : là où on affiche la progressbar
    // - 2eme argument -> sa taille max (le total des brands)
    // -----------
    $progressBar = new ProgressBar($output, count($this->brands));
    $progressBar->start();


    // -----------
    // Pour chaque brand présente dans notre tableau,
    // -----------
    foreach($this->brands as $brand) {

        // On créé une Brand et on la nomme avec notre brand en cours d'itération de la boucle,
        $brandObject = new Brand();
        $brandObject->setName($brand);

        // On persist l'objet,
        $this->em->persist($brandObject);

        // On avance la progressbar,
        $progressBar->advance();
    }

    // On a finit la boucle, on peut terminer la progressbar.
    $progressBar->finish();


    // -----------
    // On peut sauvegarder nos objets en base de données
    // -----------
    $this->em->flush();


    // -----------
    // On informe l'utilisateur que tout s'est bien passé
    // -----------
    $output->writeln('');
    $output->writeln('<info>Brands created with success !');
    return command::SUCCESS;
}
```


Notre commande est prête, si on la lance on obtient : 

```bash
php bin/console brands


Creating all brands...
11/11 [============================] 100%
Brands created with success !
```

### La commande Category

```bash
php bin/console make:command

Choose a command name (e.g. app:grumpy-pizza):
> categories
```
?> Un fichier `CategoriesCommand.php` est généré dans le dossier `src/Command`.

> Cette commande est sensiblement la même à ceci près qu'on envoie des données différents dans l'objet catégorie, on la rempli donc quasi comme pour les brands

### La commande Model

```bash
php bin/console make:command

Choose a command name (e.g. app:grumpy-pizza):
> models
```
?> Un fichier `ModelsCommand.php` est généré dans le dossier `src/Command`.

Ici on va **importer des models** et les **associer avec les catgories et les brands** qu'on aura déjà créés.<br>
On va faire ça en passant par un **Service**.

#### Creation du service

Direction `src/Service` *(si ce dossier n'existe pas on le créé)* où on créé un fichier `ModelService.php` :

```php
<?php
namespace App\Service;

use App\Entity\Model;
use App\Repository\BrandRepository;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ModelService.php
 */
class ModelService
{
    /**
     * @var CategoryRepository $categRepository
     */
    private CategoryRepository $categRepository;

    /**
     * @var BrandRepository $brandRepository
     */
    private BrandRepository $brandRepository;

    /**
     * @var EntityManagerInterface $em
     */
    private EntityManagerInterface $em;

    /**
     * ModelService constructor.
     * @param CategoryRepository $categRepository
     * @param BrandRepository $brandRepository
     * @param EntityManagerInterface $em
     */
    public function __construct(
        CategoryRepository $categRepository,
        BrandRepository $brandRepository,
        EntityManagerInterface $em
    ) {
        $this->categRepository = $categRepository;
        $this->brandRepository = $brandRepository;
        $this->em = $em;
    }

    /**
     * @return array
     */
    private function getModels(): array
    {
        $arrayModel['Audi'] = [
            'A5' => 'Berline',
            'Batmobile' => 'Tank',
            'Bumbo' => 'Mini',
            'R8' => 'Coupée',
        ];
        $arrayModel['Ford'] = [
            'Falcon' => 'Mad Max',
            'Gran Torino' => 'Starsky et Hutch',
            'KITT' => 'K2000',
        ];
        $arrayModel['Renault'] = [
            'Clio' => 'Citadine',
            'Kangoo' => 'Utilitaire',
            'Scenic' => 'Monospace',
            'Twingo' => 'Citadine',
        ];
        $arrayModel['CTR'] = [
            'Trikee' => 'Kart',
            'Retro Prime' => 'Kart',
        ];
        $arrayModel['Volkswagen'] = [
            'Touran' => 'Monospace',
            'Tiguan' => 'SUV',
            'Polo' => 'Polyvalente',
            'Golf' => 'Compacte',
            'Passat' => 'Familiale routière'
        ];
        $arrayModel['Ferrari'] = [
            'Roma' => 'Coupée Sport'
        ];
        $arrayModel['Mercedes'] = [
            'Classe A' => 'Berline',
            'EQA' => 'SUV',
            'Classe V' => 'Monospace',
        ];
        return $arrayModel;
    }

    /**
     * @return int
     */
    public function getModelsSize(): int
    {
        return sizeof($this->getModels());
    }

    /**
     * Appeler depuis la commande
     */
    public function createModels() {

        // -----------
        // On itère sur notre tableau de Clé, Valeur via getModels()
        // Où la Clé = nom de la marque
        // Où la Valeur = tableau de Clé, Valeur entre nom du modèle et de la catégorie
        // -----------
        foreach ($this->getModels() as $brandName=> $arrayInfosModels) {

            // -----------
            // On itère sur notre tableau de Clé, Valeur
            // Où la Clé = nom du modèle
            // Où la Valeur = nom de la catégorie
            // -----------
            foreach ($arrayInfosModels as $modelName => $categoryName) {

                // -----------
                // On peut donc récupérer en BDD nos catégories et 
                // nos marques respectives en fonction
                // des données de notre tableau
                // -----------
                $category = $this->categRepository->findOneBy(['name' => $categoryName]);
                $brand = $this->brandRepository->findOneBy(['name' => $brandName]);

                // -----------
                // Afin de pouvoir créer nos Model
                // -----------
                $model = (new Model())
                    ->setName($modelName)
                    ->setBrand($brand)
                    ->setCategory($category)
                ;
                $this->em->persist($model);
            }
        }
        $this->em->flush();
    }
}
```

?> Ce service nous permet d'ajouter nos models et de les associer aux brands/categories

#### Creation de la command

Notre commande ressemble à ça : 

```php
<?php

namespace App\Command;

use App\Service\ModelService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ModelsCommand extends Command
{
    protected static $defaultName = 'models';
    protected static $defaultDescription = 'Add models to your Database';

    /**
     * @var ModelService $modelService
     */
    private ModelService $modelService;

    /**
     * ModelCommand constructor.
     * @param ModelService $modelService
     */
    public function __construct(ModelService $modelService)
    {
        $this->modelService = $modelService;
        parent::__construct();
    }

    /**
     * Allow to execute an other Command
     * @param InputInterface $input
     * @param OutputInterface $output
     * @param string $command
     * @return int
     * @throws Exception
     */
    protected function executeCommand(InputInterface $input, OutputInterface $output, string $command): int
    {
        $command = $this->getApplication()->find($command);
        return $command->run($input, $output);
    }

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        // -----------
        // On execute d'abords la command Brands
        // Pour s'assurer qu'elles seront bien présentes
        // lors de l'ajout des models
        // -----------
        $this->executeCommand($input, $output, 'brands');
        $output->writeln('------------');

        // -----------
        // Même punition pour les categories
        // -----------
        $this->executeCommand($input, $output, 'categories');
        $output->writeln('------------');

        // -----------
        // On appelle le service via createModels()
        // -----------
        $this->modelService->createModels();

        // -----------
        // On est trop forts
        // -----------
        $output->writeln('<info>Everything is created with success !');
        return command::SUCCESS;
    }
}
```

Si on tape la commande on a ça : 

```bash
Creating all brands...
 11/11 [============================] 100%
Brands created with success !
------------
Creating all categories...
 26/26 [============================] 100%
Categories created with success !
------------
Creating all models...
Everything is created with success !
```