# Exemple avec Music-Shop

Projet de gestion d'Artistes, d'albums, Genres et autres Songs...<br>
Beeaauucoup plus d'informations ci-dessous **&darr;** :eyes:

## Arborescence du projet

Le projet contiendra le fichiers suivants : 

![](img/phpobj_example-musicShop-arborescenceProjet.png)


### Le fichier **TraitName.php**

```php
<?php

trait TraitName
{
    private $name;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

}
```

?> `TraitName` est un **attribut** dont on sait qu'il sera **utilisé à plusieurs endroits**, hors, quand un attribut est utilisé plusieurs fois on créé un **trait** qui stocke les informations de cet attribut pour pouvoir l'exploiter dans les classes où cet attribut sera necessaire. <br>
*(pourquoi faire du copier/coller relou à maintenir quand on a de belles slutions comme ça ? :sunglasses:)*

### Le fichier **TraitYearName.php**

```php
<?php

trait TraitYearName
{
    private $year;

    use TraitName;

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @param mixed $year
     */
    public function setYear($year): void
    {
        $this->year = $year;
    }

}
```

?> `TraitYearName` est, comme son comparse `TraitName`, un **attribut** qui sera **utilisé à plusieurs endroits**. Du coup même topo, on fait un trait. <br>
*On notera que ce dernier exploite le trait `TraitName`.* <br>
*Du coup, en important `TraitName`, on ajoute les attributs `name` et `year` en une seule fois*

### Le fichier **Genre.php**

```php
<?php

class Genre
{

    private int $id;

    use TraitName;

    /**
     * Genre constructor.
     */
    public function __construct()
    {
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

}
```

?> Une des plus petites classes, **Genre** ne prends qu'un `id` et le `TraitName`, il n'a même pas de `__toString`<br>
Il sera exploité dans d'autres classes.


### Le fichier **Song.php**

```php
<?php

class Song
{

    private int $id;
    private string $duration;
    private float $price;

    use TraitName;



    /**
     * Song constructor.
     */
    public function __construct()
    {
    }



    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDuration(): string
    {
        return $this->duration;
    }
    /**
     * @param string $duration
     */
    public function setDuration(string $duration): void
    {
        $this->duration = $duration;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }
    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }



    public function __toString(): string
    {
        return $this->name . ' (' . $this->duration . ') -> ' . $this->price . '€';
    }

}
```

?> Un peu plus complet que **Genre**, la classe **Song** prends un `id`, un `duration`, un `price`et le `TraitName`.<br>
Il a même un beau `__toString` 

### Le fichier **Artist.php**
```php
<?php

class Artist
{
    private int $id;
    private string $nationality;
    private array $arrayGenres;

    use TraitYearName;



    /**
     * Artist constructor.
     */
    public function __construct()
    {
        $this->arrayGenres = array();
    }



    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNationality(): string
    {
        return $this->nationality;
    }
    /**
     * @param string $nationality
     */
    public function setNationality(string $nationality): void
    {
        $this->nationality = $nationality;
    }

    /**
    * @return array
    */
    public function getArrayGenres(): array
    {
        return $this->arrayGenres;
    }
    /**
     * @param Genre $genre
     */
    public function addGenre(Genre $genre)
    {
        $this->arrayGenres[] = $genre;
    }



    public function __toString(): string
    {
        $string = '<strong>' . $this->name . '</strong><br>';
        $string .= $this->nationality . '<br>';
        $string .= '<i>' . $this->year . '</i></br>';
        if (!sizeof($this->getArrayGenres()) == 0)
        {
            $string .= '<h4>Genres de l\'artiste : ' . '</h4>';
            foreach($this->getArrayGenres() as $genre)
            {
                $string .= '- '. $genre->getName() . '</br>';
            }
        }
        else
        {
            $string .= 'Aucun genre pour l\'artiste <br>';
        }
        return $string;
    }
}
```
?> **Artist** est déjà plus avancé. Il prends en attributs un `id`, un `nationality`, un `arrayGenres`et le `TraitYearName`. <br>
Il exploite aussi la classe **Genre** au travers de la fonction `addGenre()`.<br>
Son `__toString`est lui aussi plus poussé avec, nottament, un `foreach` qui affiche les données de l'array des genres.


### Le fichier **Album.php**

```php
<?php

class Album
{
    private int $id;
    private int $isParentalAdvisory;
    private float $price;
    private array $arraySongs;
    private Artist $artist;
    private array $arrayGenres;

    use TraitYearName;



    /**
     * Album constructor.
     */
    public function __construct()
    {
    }




    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getIsParentalAdvisory(): int
    {
        return $this->isParentalAdvisory;
    }
    /**
     * @param int $isParentalAdvisory
     */
    public function setIsParentalAdvisory(int $isParentalAdvisory): void
    {
        $this->isParentalAdvisory = $isParentalAdvisory;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }
    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }



    /**
     * @return Artist
     */
    public function getArtist(): Artist
    {
        return $this->artist;
    }
    /**
     * @param Artist $artist
     */
    public function setArtist(Artist $artist): void
    {
        $this->artist = $artist;
    }

    /**
     * @return array
     */
    public function getArraySongs(): array
    {
        return $this->arraySongs;
    }
    /**
     * Add the $song to the array $songs
     * @param Song $song
     */
    public function addSong(Song $song): void
    {
        $this->arraySongs[] = $song;
    }

    /**
     * @return array
     */
    public function getArrayGenres(): array
    {
        return $this->arrayGenres;
    }
    /**
     * Add the $genre to the array $arrayGenres
     * @param Genre $genre
     */
    public function addGenre(Genre $genre): void
    {
        $this->arrayGenres[] = $genre;
    }


    
    public function __toString(): string
    {
        $string = '<h3>Nom de l\'album : ' . '</h3>';
        $string .= '<strong>' . $this->name . '</strong> (' . $this->year . ') -> ' . $this->price . '€</br>';

        $string .= '<h3>Artiste de l\'album : ' . '</h3>';
        $string .= $this->artist. '</br>';

        $string .= '<h3>Chansons de l\'album : ' . '</h3>';
        foreach($this->getArraySongs() as $song)
        {
            $string .= '- '. $song . '</br>';
        }
        $string .= '<h4>Genres de l\'album : ' . '</h4>';
        foreach($this->getArrayGenres() as $genre)
        {
            $string .= '- '. $genre->getName() . '</br>';
        }

        return $string;
    }

}
```

?> **Album** prends en attributs un `id`, un `isParentalAdvisory`, un `price`, un `arraySongs`, un `artist`, un `arrayGenres`et le `TraitYearName`.<br>
Il exploite la classe **Genre** au travers de la fonction `addGenre()` mais fait aussi la même chose avec **Song** *(via `addSong()`)* et **Artist** *(via `getArtist()`, ici il ne s'agit pas d'un array contrairement aux 2 autres)*<br>
Son `__toString` utilise aussi des `foreach` qui affiche les genres et les songs.


### Le fichier **MusicShopFactory.php**

```php
<?php

include 'TraitName.php';
include 'TraitYearName.php';

include 'Album.php';
include 'Genre.php';
include 'Song.php';
include 'Artist.php';


class MusicShopFactory
{
    /**
     * @param string $name
     * @param int $publishedYear
     * @param int $isParentalAdvisory
     * @param float $price
     * @return Album
     */
    public function createAlbum(string $name, int $publishedYear, int $isParentalAdvisory, float $price) : Album
    {
        $album = new Album();
        $album->setName($name);
        $album->setYear($publishedYear);
        $album->setIsParentalAdvisory($isParentalAdvisory);
        $album->setPrice($price);
        return $album;
    }

    /**
     * @param string $name
     * @param string $duration
     * @param float $price
     * @return Song
     */
    public function createSong(string $name, string $duration, float $price) : Song
    {
        $song = new Song();
        $song->setName($name);
        $song->setDuration($duration);
        $song->setPrice($price);
        return $song;
    }

    /**
     * @param string $name
     * @param string $nationality
     * @param int $beginningYear
     * @return Artist
     */
    public function createArtist(string $name, string $nationality, int $beginningYear ) : Artist
    {
        $artist = new Artist();
        $artist->setName($name);
        $artist->setNationality($nationality);
        $artist->setYear($beginningYear);
        return $artist;
    }

    /**
     * @param string $name
     * @return Genre
     */
    public function createGenre(string $name) : Genre
    {
        $genre = new Genre();
        $genre->setName($name);
        return $genre;
    }



    public function printDatas(): void
    {
        // Création de l'album
        $album = $this->createAlbum('Master of puppets', 1986, 0, 14.99);

        // Création et Ajout des chansons à l'album
        $album->addSong($this->createSong('Battery', '00:05:12', 1.19));
        $album->addSong($this->createSong('Master of puppets', '00:08:35', 1.19));
        $album->addSong($this->createSong('The thing that should not be', '00:06:36', 1.19));
        $album->addSong($this->createSong('Welcome home', '00:06:27', 1.19));
        $album->addSong($this->createSong('Disposable heroes', '00:08:17', 1.19));
        $album->addSong($this->createSong('Leper messiah', '00:05:40', 1.19));
        $album->addSong($this->createSong('Orion', '00:08:27', 1.19));
        $album->addSong($this->createSong('Damage, Inc.', '00:05:32', 1.19));

        // Création de l'artiste
        $artiste = $this->createArtist('Metallica', 'American', 1982);

        // Set l'artiste à l'album
        $album->setArtist($artiste);

        // Ajout des genres à l'album
        $heavy = $this->createGenre('Heavy metal');
        $album->addGenre($heavy);
        $thrash = $this->createGenre('Thrash metal');
        $album->addGenre($thrash);
        $album->addGenre($this->createGenre('Hard Rock'));

        echo $album . '<br>';
    }
}
```

?> **MusicShopFactory** est une classe particulière. Elle peux être vue comme une classe "ciment" qui permet de lier toutes les classes entre-elles.  Cela évite, entre autres, d'avoir des `includes` de partout, *faut le voir comme une classe 'Main.php'*
<br><br><br>
**Que fait-elle, plus précisément ?**<br><br>
Tout d'abord, elle regroupe toutes les classes et tout les traits créés juqu'à maintenant via les `include`.<br><br>
Ensuite, on y créé 4 fonctions des création *(`createAlbum()`, `createSong()`, `createArtist()` et `createGenre()`)*<br><br>
Ces fonction permettent, comme leur nom l'indique, de créer des Albums, des Songs, des Artistes et des Genres en exploitant les methodes `setter` de chacunes des classes concernées.<br><br>
Une dernière fonction `printDatas()`, va utiliser l'ensemble de ces fonctions.<br>
On va :<br>
• créer un Album sous forme de variable,<br>
• créer des Chansons tout en les attribuants à l'Album<br>
• créer un Artiste sous forme de variable<br>
• ajouter l'Artiste à l 'Album<br>
• créer des genres sous forme de variables<br>
• ajouter ces genres à l'Album<br>
<br>
Une fois tout ça fait, on va pouvoir afficher tout ça, faisons-le dans un fichier **index.php**.


### Le fichier **Index.php**

#### **1ère version** <br> &rarr; Utilisation de la fonction `printDatas()`

Contenu du fichier :

```php
<?php

include 'MusicShopFactory.php';

// On instancie la classe musicShopFactory
$musicShopFactory = new MusicShopFactory();

// On accède à la fonction printDatas() qui se trouve
// dans la classe musicShopFactory
$musicShopFactory->printDatas();
```
?> Dans cette version, on fait appel à la fonction `printDatas()` qui exploite les différents `createEtc...()` au sein de la classe MusicShopFactory *(elle utilise les `$this`)*.<br><br>
Le résultat ressemble à ça :
![](img/phpobj_example-musicShop-export1.png)


#### **2ème version** <br> &rarr; Utilisation des fonctions `create`*etc* comme au sein de `printData()`

Contenu du fichier :

```php
<?php

include 'MusicShopFactory.php';

// On instancie la classe musicShopFactory
$musicShopFactory = new MusicShopFactory();

// On accède aux fonctions createAlbum(), createSong(),
// createArtist() et createGenre() qui se trouve dans la classe
// musicShopFactory

// Création de l'album
$album = $musicShopFactory->createAlbum('Master of puppets', 1986, 0, 14.99);

// Ajout des chansons à l'album
$album->addSong($musicShopFactory->createSong('Battery', '00:05:12', 1.19));
$album->addSong($musicShopFactory->createSong('Master of puppets', '00:08:35', 1.19));
$album->addSong($musicShopFactory->createSong('The thing that should not be', '00:06:36', 1.19));
$album->addSong($musicShopFactory->createSong('Welcome home', '00:06:27', 1.19));
$album->addSong($musicShopFactory->createSong('Disposable heroes', '00:08:17', 1.19));
$album->addSong($musicShopFactory->createSong('Leper messiah', '00:05:40', 1.19));
$album->addSong($musicShopFactory->createSong('Orion', '00:08:27', 1.19));
$album->addSong($musicShopFactory->createSong('Damage, Inc.', '00:05:32', 1.19));

// Création de l'artiste
$artiste = $musicShopFactory->createArtist('Metallica', 'American', 1982);

// Set l'artiste à l'album
$album->setArtist($artiste);

// Ajout des genres à l'album et à l'artiste
$heavy = $musicShopFactory->createGenre('Heavy metal');
$album->addGenre($heavy);
$artiste->addGenre($heavy);
$thrash = $musicShopFactory->createGenre('Thrash metal');
$album->addGenre($thrash);
$artiste->addGenre($thrash);
$album->addGenre($musicShopFactory->createGenre('Hard Rock'));
$artiste->addGenre($musicShopFactory->createGenre('Speed Metal'));

echo $album . '<br>';
```
?> Dans cette version, on fait sensiblement la même chose que dans la fonction `printDatas()`, toutefois on ne fait pas appel à elle.<br>
On exploite directement les différents `createEtc...()` mais en dehors de la classe MusicShopFactory *(d'où l'utilisation des `$musicShopFactory` à la place des `$this`)*.<br>
Petite différence également, on ajoute les genre à l'album mais aussi à l'artiste contrairement à la version précédente.<br><br>
Le résultat ressemble à ça : <br>
*On ne récise pas à nouveau l'origine des `__toString`, c'est pareil qu'en haut*<br>
![](img/phpobj_example-musicShop-export2.png)


## Visualisation des classes du projet

Maintenant que tout est créé, on peux se rendre compte de ce que donne ce projet en afficant les trait, les classes, ainsi que leurs attributs et methodes  :

![](img/phpobj_example-musicShop-visualisationProjet.png)
