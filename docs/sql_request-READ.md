## Lecture dans une table

Pour **sélectionner des entrées** dans les tables on peux être ammené à taper une des commandes suivantes :

## Les commandes

### Selection *SELECT*

```sql
SELECT *
FROM voiture
WHERE couleur LIKE 'blanc%';
```

?> On chope toutes les infos concernant les voitures dont la couleur commence par **blanc**

> Le **étoile** selectionne la totalité des champs de la table, on peux en cibler une ou plusieurs si on veux en les citant et en les séparant par des virgules s'ils y en a plusieurs.

> Dans un **SELECT**, au niveau du **WHERE**, on peut mettre des `%` pour avoir juste une partie de la recherche.<br><br>
> *Exemple: on a des voitures Gris foncé, Bleu foncé, et que l'on veux tout ce qui termine par foncé, on mets `%foncé` dans notre requête pour tous les avoir.* :ok_hand:

<br>

### Ordonner *ORDER BY*

```sql
SELECT *
FROM contacts
WHERE birth_at IS NOT NULL
ORDER BY birth_at DESC;
```

?> On chope les contacts dont la date de naissance n'est pas `NULL` <br>
On **ordonne/classe** lee resultats par date de naissance en partant du plus jeune grace au `ORDER BY` et au `DESC`.

<br>

### Limiter les resultats *LIMIT*

```sql
SELECT *
FROM contacts
WHERE birth_at IS NOT NULL
ORDER BY birth_at ASC
LIMIT 10, 15;
```

?> On chope les contacts dont la date de naissance n'est pas `NULL` et qui est le plus vieux.<br>
On **limite** le nombre de resultats à **15 à partir du 10ème** grace au `LIMIT`.

<br>

### Groupe *GROUP BY*

```sql
SELECT count(*), sexe
FROM cours.students
GROUP BY sexe;
```

?> On recupère et on **groupe** tous students par sexe en nous donnant le **nombre présent par sexe**

<br>

### Concaténation *CONCAT()*

```sql
SELECT CONCAT(students.last_name, ' - ', students.first_name)
AS "Nom étudiant"
FROM 'students'
```

?> Retourne les champs en les concaténant avec `CONCAT()` et des **virgules**. <br>
On aura alors ***Parker - Peter*** par exemple en résultat.

<br>

### Groupe et Concaténation *GROUP_CONCAT()*

`GROUP_CONCAT` permet de concatener les informations du champs en paramètre en une seule colonne, *il permet d'éviter la redondance de lignes de même type*

```sql
SELECT album.name, GROUP_CONCAT(artist.name)
  
FROM album

JOIN album_artist
ON album.id = album_artist.album_id

JOIN artist
ON artist.id = album_artist.artist_id

GROUP BY album.id;
```

?> Affiche **par album** les **artistes ayant participé** à celui-ci. <br> 
*Les différents artistes seront regroupé dans un seul et même champs.*

<br>

### Comparaisons *=, >, etc*

```sql
SELECT *
FROM voiture
WHERE prix <= 6000
```

?> On chope les voitures dont le prix est **inférieur ou égal à 6000€**

Quelques exemple de comparaisons : 

Opérateur | Signification
------------ | ------------- 
`=` | Égal à
`!=` | Différent de
`>` | Supérieur à
`<` | Inférieur à
`>=` | Supérieur ou égal à
`<=` | Inférieur ou égal à

<br>

##### Un autre exemple avec une date :

```sql
SELECT *
FROM contacts
WHERE birth_at > '2017-05-15';
```
?> On chope les contacts dont la date de naissance est **après le 15 mai 2017**

<br>

### Comptage *count()*

```sql
SELECT count(*)
FROM voiture
WHERE modele LIKE 'Clio';
```

?> On **compte** le nombre de voitures de modèle **Clio** 

<br>

### Calculs de base *SUM etc*

On peux faire des calculs de base tels que
- Des **additions** (`SUM(paramètre)`),
- Chopper la **valeur minimum** (`MIN(paramètre)`),
- Chopper la **valeur maximum** (`MAX(paramètre)`),<br>

>Si on a plusieurs valeurs min ou max, il nous rends la première


```sql
SELECT SUM(song.price) AS 'Total prix chansons'
FROM song;
```
?> On additione la totalité des prix des chansons.

<br>


### Arrondi *round()*

```sql
SELECT ROUND(height_cm/100, 2)
FROM cours.students;
```
<br>

?> On chope une **hauteur en cm** des étudiants et on la **divise par 100** et on l'**arrondi**. Le **2** en second argument indique que l'on veux 2 chiffres après la virgule.

> On peux aussi calculer une **moyenne** avec `AVG()`.

<br>

### Aléatoire *random()*


```sql
SELECT abs(random()%100);
```

?> On génère un nombre aléatoire modulo 100<br>

> **ABS** permet de renvoyer un nombre en **valeur absolue**

> `random()` genère un nombre déterminé **aléatoirement** entre 2 valeurs prédéfinies pas SQLite/MySQL

<br>


### Cumul *AND*

```sql
SELECT *
FROM voiture
WHERE prix > 6000
AND modele LIKE 'Clio'
AND couleur LIKE 'Vert%'
```

?> On chope les voitures de modèle **Clio**, de couleur **verte** et dont le prix est **strictement supérieur à 6000€**

<br>


### Cumul *AND & OR*

```sql
SELECT count(*)
FROM voiture
WHERE (
    modele LIKE 'Clio' AND prix <= 6000
) OR (
    marque_id = 1 AND prix IS NOT NULL
)
```

?> On chope toutes les voitures : <br>
• dont le modèle est **Clio** et dont le prix est **inférieur ou égal à 6000€** <br>
OU <br>
• dont l'ID est **1** et dont le prix n'est **pas null**

> Quand on cumule des `AND` et des `OR`, le `AND` est prioritaire en général. Mais sinon, comme en math, on peux modifier tout ça avec des parenthèses .

<br>


### Dans *IN*

```sql
-- Exemple de base
SELECT last_name, height_cm
FROM cours.students
WHERE height_cm = 121 OR height_cm = 201;

-- Même chose avec un IN
SELECT last_name, height_cm
FROM cours.students
WHERE height_cm IN (121, 201);
```

> Le `IN` permets une syntaxe différente et plus souple. Il permets aussi de gérer plusieurs résultats. <br><br>
En gros, il faut se dire : <br>
*"Si j'ai une requette qui renvoie plusieurs entités, j'utilise un `In`."*

Autre exemple : 

```sql
SELECT *
FROM contacts
WHERE contacts.last_name IN (
    SELECT students.last_name
    FROM students
)
AND contacts.first_name IN (
    SELECT students.first_name
    FROM students
);
```
?> Récupère les contacts ayant le même prénom et nom qu'une personne présent dans la table étudiant

<br>


### Entre *BETWEEN*

```sql
SELECT *
FROM contacts
WHERE birth_at BETWEEN '2017-05-15' AND '2018-01-01';
```

?> On chope les contacts dont la date de naissance est **entre** le 15 mai 2017 **et** le 1er Janvier 2018.

<br>


### Test d'une agrégation *HAVING*

```sql
SELECT ROUND(height_cm/100, 2) AS height_m
FROM cours.students
HAVING height_m > 1.5;
```

?> On affiche l'**arrondi de la taille** des etudiants pour ceux qui sont **plus grands** que 1,5m

>  On utilise ici le `HAVING` pour exploiter la **variable height_m** créée sur la 1ere ligne. <br>
*(le `WHERE`n'aurait rien retourné)*<br>
Le `HAVING` permet de **tester le resultat** d'une fonction SQL.

> On peux cumuler les `WHERE` et les `HAVING` *(le `HAVING` vient toujours **après** le `WHERE`).*

<br>


### Bonus combo

```sql
SELECT COUNT(*), students.sexe, students.height_cm
FROM cours.students
WHERE students.sexe = 'F'
GROUP BY students.height_cm
HAVING COUNT(*) >= 2
ORDER BY height_cm
```

?> On **recupère** et on **groupe par taille** tous les students de **sexe féminin** en nous donnant le **nombre présent par sexe** et en les **classant pas taille** et en n'affichant **que ceux qui on plus ou egal 2 resultats**.* <br>
:fire: :scream: :fire:

<br>
<hr>

## Les clés étrangères et les jointures

### Les relations entre les tables, les tables intermediaires

On créé des tables ne servant que d'**intermediaire entre 2 autres tables**, elles n'ont qu'un **id en clé principale** et **2 autres id de clés etrangères**. *On relie 2 tables entre elles pour eviter la duplication de données.*

```sql
SELECT  etudiant.nom,
	    etudiant.prenom,
        GROUP_CONCAT(matiere.nom) AS 'Matieres'

FROM    cours

JOIN    etudiant
ON  	cours.etudiant_id = etudiant.id
JOIN    matiere
ON	  cours.matiere_id = matiere.id

GROUP BY etudiant.id
```

?> Ici on passe par un **table intermediaire** avec la jointure entre les tables **etudiant** et **matiere**<br><br>
Pour cela **on part de la table cours**, on **choppe la table etudiant via etudiant_id**, et ensuite on **choppe la table matière à partir de la table cours via matiere_id**.

<br>


### *JOIN* et *INER JOIN*

Fais la jonction entre 2 tables différentes en passant par la clé étrangère via le `ON` *(on précise la clé ensuite)* pour faire une commande.

Le `INNER JOIN` est sensiblement la même chose que le `JOIN`. Les deux sont pareils.

- Il fait l'union entre les 2 tables et affiche les informations **communes** aux 2 tables.<br>
- Il n'affiche que les resultats où il y a des **données communes** *(je me répète mais c'est bien ça)*.<br>
- Il est **strict** sur les données à afficher.

##### Exemples :

```sql
SELECT *
FROM contacts
INNER JOIN ville
ON contacts.ville_id = ville.id
WHERE ville.code = "DTN";
```

?> Recupère toutes les villes avec le code **DTN** dans la table **ville** à partir de la table **contacts**

<br>

```sql
SELECT etudiant.nom, etudiant.prenom, matiere.nom
FROM etudiant

JOIN cours
ON etudiant.id = cours.etudiant_id
JOIN matiere
ON matiere.id = cours.matiere_id
```

?> Recupère les **nom et prénoms des etudiants** en partant de la table etudiant ainsi que le **nom des matières**, pour cela il fait la jointure de **etudiant vers matiere**

<br>

### *LEFT JOIN* et *RIGHT JOIN*

Le `LEFT JOIN` affiche **toutes les informations de la table de gauche *(celle qui sert de point de départ pour le `JOIN`)***. <br>
Contrairement au `JOIN` vu juste au dessus, il affichera, **en plus des informations communes aux 2 tables**, **la totalité des infos contenues dans la table qui sert de départ** pour le `JOIN` *(là aussi, je me répète mais il vaut mieux)*.

**La syntaxe :** 

```sql
SELECT *
FROM personne

LEFT JOIN adresse
ON adresse.personne_id = personne.id;
```

> `RIGHT JOIN` fera la même chose que le `LEFT JOIN` à la différence près que, à l'inverse du LEFT, il affichera, **en plus des informations communes aux 2 tables**, **la totalité des infos contenues dans la table qui sera ciblée** pour le `JOIN`.

