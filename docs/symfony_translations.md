# Translations

Package nécessaire :

```bash
composer require symfony/translation
```

On a le package qui gère les **translations** *(ou le mutlilangue hein)*, avec un fichier de config `translation.yaml` dans le dossier `config/packages`.

Il a cette tête : 

```yaml
# config/packages/translation.yaml
framework:
    default_locale: fr
    translator:
        default_path: '%kernel.project_dir%/translations'
        fallbacks:
            - fr
```

?> Si notre site est en **fr**, bien penser à adapter le `default_locale` *(par défaut c'est **en**)*<br>
Si on ajoute d'**autres langues**, il faut les mettre à la suite dans `fallbacks`.


Maintenant on va ajouter un fichier `messages.fr.yaml` dans le dossier `translations` qui contiendra **les valeurs de nos clés de traductions**.

Il a cette tête : 

```yaml
students:
  index:
    name: 'Nom'
    first_name: 'Prénom'
    height: 'Taille'
    gender: 'Genre'
    detailled: 'Détails'
```

?> Ici on rentre des valeurs pour les clés `name`, `first_name`, `height`, `gender` et `detailled` dans `index`, lui-même dans `students`.

> Pour avoir l'**équivalence** avec une autre langue, on ajoute un fichier `messages.en.yaml` pour l'anglais par exemple et on le rempli avec les **même clés** et leur valeurs en anglais *(`name: 'Name'` par exemple)*.

Pour appeler ces valeurs, on le fait dans nos vues en twig de cette manière : 

```html
<p>{{ 'students.index.name' | trans}}</p>
<p>{{ 'students.index.first_name' | trans}}</p>
<p>{{ 'students.index.height' | trans}}</p>
<p>{{ 'students.index.gender' | trans}}</p>
<p>{{ 'students.index.detailled' | trans}}</p>
```

!> Ne pas oublier le ` | trans` à la suite