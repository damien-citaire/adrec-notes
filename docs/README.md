# ADREC notes<br> **Concepteur d'applications**


## C'est quoi ?

Il s'agit de l'ensemble des notes prise pendant et à partir des cours donnés durant le cursus 2020/2021 de la formation Concepteur d'Application à l'ADREC.

Les cours nécésitant les plus de prise de notes et de ressources à exploiter pedant et après ce cursus sont listé dans la sidebar.<br>
De nombreux exemples et échantillons de codes à ré-exploiter agrémenteront ces notes.

## C'est pour quoi ?

C'est pour être en mesure de revoir et de mieux assimiler la quantité incroyable de connaissances partagées pendant ce cursus.

C'est aussi pour se faire une petite bibliothèque de ressource en dev' qui pourront être ré-exploitées sans modération.

## C'est parti ?

Bah allez, go menu à gauche alors :thumbsup: