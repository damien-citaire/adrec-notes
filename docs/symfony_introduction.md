# Symfony

## Presentation

Symfony est un **framework PHP**, il se base sur le **modèle de conception MVC** *(Model, View, Controller)*.

## Bibliothèque/Framework

**Symfony** est une **bibliothèque de classes**, de **fonctionnalités basiques**. Il évite de recoder ces fonctionnalités à chaque projet.

![](img/symfony_framework.png)

## MVC

**Patron de conception *(Design Pattern)*** qui permets de gérer de manière **propre** le projets.

- On a un **Controller** qui nous dit quels sont les **Models** à afficher,<br>

- Le **Model** fournit les informations,<br>
Il correspond au **tables/classes** de l'application.

- La **Vue** envoie les instructions au **Controller** qui transmets au **Model**. <br>
En gros, la vue est ce que l'utilisateur va voir, l'interface, le front.<br>
La vue ne se contente que d'**afficher les données**, si on y fait une requête SQL par exemple, c'est que quelque chose ne vas pas.

![](img/symfony_mvc-design-patern.png)

## Versioning de Symfony

Symfony suit le **Semantic Versioning 2.0.0**.

- Le 1er chiffre correspond à une **version majeure** *(possibilité d'incompatibilité avec une version d'un autre chiffre majeur)*,
- Le 2eme chiffre, **version mineure** *(ajout de fonctionnalités mineures ne créant pas de breaking changes)*,
- Le 3eme chiffre, c'est quand on **patch**, qu'on **corrige un bug**.

### Faire une migration

Avant de passer d'une **version majeure** à une autre, il faut d'abord se mettre sur la **dernière version mineure de la version majeure où on se trouve**.<br>
Il faut ensuite faire le menage dans les fonctionnalités qui seront **depreciées** sur la **version majeure suivante**.<br>
On peux enfin passer sur la version majeure suivante *(et bien **tester** surtout)*.

## Arborescence d'un projet Symfo

#### Le fichier `.env`
C'est le fichier environement dans lequel on defini l'environement où on se trouve *(dev, prod...)*.<br>
On y renseigne aussi d'autres informations, comme la configuration d'**accès à la BDD du projet** *(Au niveau de `DATABASE_URL`)*.

!> `.env` est un fichier **critique**, on ne le **git pas**.<br>
On peux le **dupliquer** et seulement **giter une version sans les infos de la BDD** *(sous le nom `.env.dist` par exemple)*.


<hr>

#### Le dossier `vendor` 

C'est le dossier où sont répertoriées les différentes **dépendances du projet** *(Il ne faut pas le modifier directement !)*

!> Symfony est doté d'un **autoload**, il importe **directement** tout ce qui est **nécessaire pour le fonctionnement d'une classe**.

<hr>

#### Le dossier `templates` 

Il s'agit du dossier de stockage des **Vues** de l'application *(html/css etc)*.<br>
On y trouve des fichiers **Twig**

> **C'est quoi Twig ?**<br>
Il s'agit du **générateur** de template utilisé pour Symfony. Il utilise une **syntaxe** qui lui est propre et qui sera dévellopée plus loin dans ce cours.

<hr>

#### Le dossier `src/Entities` 

Corresponds aux **classes métier** de notre application, les **Models**

> **C'est quoi Métier ?**<br>
Tout ce qui à besoin d'être **affiché** dans l'app' et qui est **necessaire à son fonctionnement**.

<hr>

#### Le dossier `src/Controller` 

Corresponds aux **Controller** de notre application

<hr>

#### Le dossier `migrations` 

Il s'agit du dossier où seront stockées les **migrations** effectués par Doctrine.
Une migration corespond à un **schéma SQL** qui résulte de la **différence entre votre base de données et vos entités**.

> **C'est quoi Doctrine ?**<br>
C'est l'**ORM** de Symfony *(Object Relational-Mapping)*, il utilise le **DQL** *(Doctrine Query Language)* comme langage pour **requêter en base de donnée**.

Tu en veux plus ? Suis ce [lien](https://symfony.com/doc/current/bundles/DoctrineMigrationsBundle/index.html)
