
# Les modules

## Présentation

Quand on charge des scripts js, l'ordre est important, mais on peux s'en affranchir en utilisant les **modules**.

Un fichier JS = un **module**<br>
Un module peux avoir **plusieurs** exports si on veux.

Un module peux exporter des **variables** ou des **fonctions** par exemple.

**Comment on fait ?**

```html
<script src="index.js"></script>
```

?> Ici on importe notre fichier de script de manière "classique"

Pour importer ce script comme un module on rajoute juste la propriété `type="module` : 

```html
<script src="index.js" type="module"></script>
```

Grace aux modules on peux **éclater notre code en pleins de fichiers** si on veux et module vas tout **reordonner** pour **eviter les erreurs d'import** :ok_hand: 

!> Attention, si on ouvre directement un fichier html qui load le script en `type="module"` dans le navigateur il ne sera pas content.<br>
Il faut bien penser à passer par un **serveur** http pour que ça marche.<br>
Pour lancer rapidement un serveur on peut utiliser la commande php (`php -S ...`) mais il faut bien préciser qu'on cible le **index.html** car par defaut il watch les fichiers **index.php**

<br>

On peux importer des fonctions js qui sont dans d'autres fichiers via les `imports`.<br>
La syntaxe pour ecrire une fonction et la rendre disponible (l'**exporter**):
```js
export function createPElement(text, id) {
    const $p = document.creatElement('p');
    $p.id = id;
    $p.innerText = text;

    return $p;
}
```

La syntaxe pour importer *(on est dans un autre fichier)* :
```js
import { createPElement } from "./chemin/vers/fichier.js;
```

Si on veux exporter **plusieurs** variables d'un coup d'un seul fichier, on peux utiliser cette syntaxe : 

```js
export const $body = document.querySelector('body');
const $container = document.querySelector('div.container');
const number = 42;

export {
    $container,
    number,
};
```

?> Ici, on exporte **3 variables** de **2 façons différentes**.

Elles pourront être importées **de la même manière** dans un autre fichier : 

```js
import {$body, number} from './variables.js';
```

## Petit reglage à la cool sur PhpStorm

Dans php Storm, dans les **prefs**, on va dans **Editor / Code Style / Javascript**, ya un onglet **import** et on coche **use file extension in module name**.<br> Ca permets d'avoir les noms des extensions dans les imports.

!> Faut bien faire gaffe à ce que ces paramètres soient appliqués seulement sur le projet **specifiquement** *(dans **Scheme** au dessus)*
