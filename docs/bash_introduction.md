# Interface Homme Machine<br> **Bash**

*Introduction effectuée sur un Debian*



## Anatomie

```bash
root@debian: ~/# ls -l ../folder
```

- `root` = le nom du user,
- `debian` = le nom de la machine,
- `~/` = le chemin où on se trouve, <br> *Ici, il s'agit du dossier racine de l'utilisateur actuel (symbolisé par le tilde **~**)*
- `#` = représentation de l'utilisateur, <br> *Ici, l'utilisateur **root** est symmbolisé par un `#`, si ça avait été un utilisateur **classique** ça aurait été un `$`*
- `root@debian:~/#` = le prompt,
- `ls` = une commande,
- `-l` = une option, <br> *Ici il s'agit d'une **option courte** (une lettre). <br> On peux les cumuler (`-al` par exemple). <br> Il existe aussi des options longues (`--color` par exemple)*
- `../folder` = un argument