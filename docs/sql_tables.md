# Anatomie des BDD

## Les tables et les champs 

Ils structurent les données. *On créer des tables (voiture), on ajoute des champs pour ce qui la compose (marque, couleur, etc)*.

Il y a dfférents type des champs : 
- **INTEGER** (entiers)
- **TEXT** (chaîne de caractères)
- **BLOB** (pour le stockage de documents, conversion de document en chaîne de caractères)
- **REAL** (chiffres à vigules)
- **NUMERIC**...

Quand on ajoute un champ, on peux préciser qu'on veux un **non-null** *(on ne veut pas que le champs soit vide, il est obligatoire)*.<br>
On peux également attribuer une **valeur par défaut** ou faire en sorte qu'il s'**auto-incrémente** *(pour les id par exemple)*

## Les index ou ID

Champs optimisés pour la recherche, permets de filtrer plus rapidement.<br>
C'est en général le **premier champs** qu'on renseigne quand on créer une table.

Un index est une clé primaire, **un champs de la table** sera cette fameuse **clé primaire**-index.<br>
L'ID à aussi une **incrémentation automatique**, il sera généré automatiquement.

> Cet identifiant est donc un **entier**, il est **unique**, forcément **non-null** et **auto-icrémental**.<br>

## Les clés étrangères

Il s'agit de la **clé primaire d'une autre table**, elle permet à différentes tables de **communiquer ensemble**. Elle est seulement en **non-null**.

On utilise donc une **clé etrangère** pour relier ce champs au champs de la **clé primaire** de la table désirée.


