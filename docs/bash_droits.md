# Les droits

### Les bases

**Les droits disponibles**: **r** READ __ **w** WRITE __ **x** EXECUTE 

**Les types/groupes d'utilisateurs**: **u** USER __ **g** GROUP __ **o** OTHERS 

##### Exemple

Quand on fait un `ls -l`, on obtient ça : 

```bash
drw-r--r-- 1 root root 2464 Sept24 09:45 folder
```

La première lettre `d` nous indique qu'il s'agit d'un **dossier**. <br>
On peux avoir un `l` pour un **lien** ou encore un `-` pour un **ficher**

On a ensuite une suite de 9 caractères, découpés en 3 blocs. <br>
Le 1er bloc donne les droits de l'**utilisateur propriétaire**, le 2ème bloc donne les droits du **groupe propriétaire** et le dernier bloc donne les droit des **autres**.

Pour connaitre l'**utilisateur propriétaire** il s'agit du 1er `root` présent dans l'exemple. <br>
Pour le **groupe**, c'est le 2ème `root` dans l'exemple ci-dessus.

Pour lister l'ensemble des **utilisateurs** on fait un `cat /etc/passwd`. <br>
Pour lister l'ensemble des **groupes** on fait un `cat /etc/group`.



## Les droits, en binaire

Tous les droits | Aucun droit | Droits partiels
------------ | ------------- | -------------
rwx rwx rwx | --- --- --- | r-w --x rwx
111 111 111 | 000 000 000 | 101 001 111
 7 7 7 | 0 0 0 | 5 1 7

**Quelques exemples de droits en binaire courant :**

`chmod 777` | `chmod 755` | `chmod 744` | `chmod 750` | `chmod 751` | `chmod 740`



## Attribuer ou retirer des droits

```bash
chmod u-r foo
```

`chmod` = commande pour la gestion des droits <br>
`u` = le type d'utilisateur visé, *ici le user* <br>
`-` = retire le droit, *`+` pour rajouter* <br>
`r` = le droit concerné, *ici c'est read*

##### Exemples

Pour virer les droits à tout le monde sur le fichier **foo** <br>
`chmod a-rwx foo`

Pour donner les droits d'ecriture à l'**user** et au **group* <br>
`chmod ug-w`

> On peux enchainer les commandes en les séparant par des `;` : <br>
`chmod a-rwx foo ; chmod a+x foo`

Pour un dossier, s'il n'as pas de `r` *(droit de lecture)*, on ne peux pas lister son contenu. <br>
On peux toutefois écrire ou afficher ce qui se trouve dans un fichier ce trouvant **à l'intérieur** de ce même dossier. <br>
S'il n'a pas de `w`, on ne peux plus écrire dessus.



## Changer le propriétaire d'un fichier

```bash
chown user /tmp/foo
```

`chown` = commande pour changer de propriétaire <br>
`user` = le nouveau ppropriétaire, *ici **user*** <br>
`/tmp/foo` = le fichier concerné, *ici **foo** dans le dossier **tmp***

Si on veux que le changement impacte un dossier mais également son contenu, on rajoute l'option `-r` *(pour recursif)*.

Si on veux changer le **groupe** propriétaire, 2 options :

* On utilise la commande `chgrp`
* On passe par `chmod` en precisant **l'utilisateur:le groupe**: `chmod user:user foo`