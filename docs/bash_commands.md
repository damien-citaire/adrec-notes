# Quelques commandes

Tour d'horizon de quelques commandes courantes et utiles.

> On peux enchainer les commandes en les séparant par des `;`


### ls *_list* 

?> Liste les fichiers du dossier courant *(ou ailleurs si un chemin est passé en argument)*.

##### Options disponibles:
* `ls -l` : donne plus d'informations sur les dossiers et fichiers listés : 

    ```bash
    drwxr-xr-x 2 root root 4096 Sept24 09:45 backup
    lrwxrwxrwx 1 root root 4096 Sept24 09:45 run -> /run
    -rw-r----- 1 root root 4096 Sept24 09:45 syslog
    drwx------ 5 root root 2464 Sept24 09:45 folder
    ``` 
* `ls -a` : liste la totalité des fichiers et dossiers <br> *Même ceux cachés (leur nom commence par un **.** )*
* `ls -h` : rends le resultat plus lisible sur l'affichage du poids <br> *2,4k au lieu de 2464 par exemple.*


### du *_disk usage*

?> Donne la taille de chaque dossier à l'endroit où on se trouve *(ou ailleurs si un chemin est passé en argument)*.

##### Options disponibles:
* `du -s` : liste seulement le total du dossier courrant sans aller plus profond
* `du -h` : rends le resultat plus lisible sur l'affichage du poids <br> *56.0k au lieu de 56 par exemple.*


### df *_disk free* 

?> Liste les disques et partitions en donnant des information sur leur utilisation de disque, *(espace libre, pourcentage, etc)*

##### Options disponibles:
* `df -h` : rends le resultat plus lisible sur l'affichage du poids, *56.0k au lieu de 56 par exemple.*


### cat *_concatenate*

?> Affiche le contenu du fichier ciblé

> `cat` a besoin de cibler un fichier en argument


### nano

?> Permets d'éditer un fichier via l'editeur de texte **nano.** <br>
Si le fichier n'existe pas, il sera créé à la fin de son édition dans nano.

> `nano` a besoin de cibler un fichier en argument


### echo

`echo "foo" > my-file` 

?> Inscrira **foo** dans le fichier **my-file** au sein du dossier courant (*sauf si un argument de chemin est passé*).

> Si le fichier **my-file** n'existe pas, il sera alors créé.<br>
> Si le fichier **my-file** existe déjà, son contenu sera alors remplacé.

<br><br>

`echo "bar" >> my-file` 

?> Inscrira **bar** <u>à la suite</u> du contenu du fichier **my-file** au sein du dossier courant (*sauf si un argument de chemin est passé*).

> Si on ne veux pas faire de retour à la ligne, on mets l'option `-n` au début de la ligne dont on veux que la suivante soit colée.

**Exemple :**

```bash
echo -n "Bonjour"
whoami
```

En sortie, si on est l'utilisateur **root**, on obtient `Bonjour root` sur une seule ligne.


### mv *_move* 

`mv my-file /var/log`

?> Déplace le fichier (ou le dossier) **my-file** dans le dossier passé en argument, *ici log dans var*.

<br><br>

`mv my-file foo`

?> Renome le fichier **my-file** en **foo** 

> Déplacer un fichier/dossier dans le même dossier reviens à le **renommer**.


### cp *_copy*

`cp my-file /tmp`

?> Permets de copier un fichier dans le dossier passé en argument

> Dans l'exemple ci-dessus, le fichier **my-file** est copié dans le dossier **tmp.**


### mkdir *_make directory*

?> Permets de créer le dossier dont le nom/chemin sera passé en argument.

> Si on veux créer plusieurs dossiers imbriqués *(tata/titi/toto)*, il faut ajouter l'option `-p` (parent), il créera alors ces 3 dossiers à la vollée.


### touch

?> Permets de créer un fichier vide dont le nom/chemin sera passé en argument.


### rm *_remove*

?> Supprime le fichier/dossier placé en argument.

> On peux utiliser l'option `-r` pour **recursif** si on tape sur un dossier qui a du contenu.


### man *_manuel*

?> Permet d'afficher les informations sur la commande passée en argument.


### --help

?> Même principe que `man` à ceci près que là il est en **option** de la commande dont on veux plus d'informations et qu'il est un peu moins complet.


### exit

?> Permets de se deconnecter de la session utilisateur en cours.


### reboot

?> Relance Debian


### history

?> Ressort toutes les commandes sorties précédement sur la session.


### clear

?> Efface toutes les commandes visibles à l'ecran.

> Si on scrolle vers le haut après un clear on retrouve les commande qui ont été tapées au dessus de l'ecran visible


### reset

?> Efface toutes les commandes de la session.


### whoami

?> Pour savoir avec quel compte on est loggé, *utile si on est sur un prompt qui ne nous l'indique pas.*


### pwd *_print working directory*

?> Pour savoir le chemin où on est, *utile si on est sur un prompt qui ne nous l'indique pas.*


### alias

`alias ll="ls -l"`

?> Permets de créer un alias **temporaire**. Dans ce cas, on dit que l'alias `ll` reviens à taper  `ls- l`.

Pour créer un alias **non temporaire**, on va dans le dossier racine de l'utilisateur *(raccourci `cd` **sans option**)* et on edite le fichier **.bashrc** <br>
Certains alias sont déjà présents mais commentés, on peux s'en inspirer pour en ajouter de nouveaux. <br>
Afin de faire prendre en compte les modifications ajoutées au fichier **.bashrc** on a 2 options :

- `source .bashrc`, 
- ou alors on tape directement dessus -> `./.bashrc`

Si on veux avoir des alias partagés entre tous les utilisateurs on peux taper sur le fichier **bash.bashrc** dans **/etc/.**


### ln *_link*

?> Permets de créer un lien **symbolique** grâce à l'option `-s`.

L'option `-s` est plus **safe** car sans cette dernière, le lien créé serait un **lien physique**. <br>

!> La différence entre un lien symbolique et un lien physique est que si l'on supprime un lien physique, le fichier original sera **également supprimé**, donc **pudence**.

`ln -s /file /tmp/file-link`

On passe donc en argument le fichier visé et on précise ensuite la destination du lien. <br> 

> Privilégier un chemin en **absolu** quand on crée un lien car si on le bouge par le suite le chemin sera tout pété.