## Construction

Le construct sert quand on on créer une nouvelle instance d'une classe *(`new`)*.

```php
$nouvelleInstanceDeClasse = new InstancedeClasse();
$nouvelleInstanceDeClasse-> getAttribute();
$nouvelleInstanceDeClasse-> setAttribute();
```
?> On créé ici une nouvelle instance à partir d'une classe existante, on recupère un de ses attributs, on modifie un de ses attributs.

Pour faire un `set`, il faut que l'**attribut de la classe soit existant**, sinon impossible.


## Heritage et surcharge

Pour l'heritage, on utilise les mot clés `extends`.
Quand on fait un **construct** dans une classe qui `extend` une autre classe, on peux rajouter des **attributs** et **exploiter ceux existants déjà** dans la classe mère.

On accède aux fonction de la classe mère avec le prefixe `parent::`<br>
**Exemple :**  `parent::__toString()`

> Si une classe mère est destinées à être extend, ses attributs qui sont normalement `private`, doivent être en `protected`. Ils seront alors accessibles pas la classe fille qui extend.

#### Exemple

Prenons une classe **Person**, qui defini une personne avec un `$firstName` et un `$lastName` :

```php
class Person
{
    protected string $lastName;
    protected string $firstName;

    /**
     * Person constructor.
     * @param string $lastName
     * @param string $firstName
     */
    public function __construct(string $lastName, string $firstName)
    {
        $this->lastName = $lastName;
        $this->firstName = $firstName;
    }

    public function __toString(): string
    {
        return 'Class Person : ' . $this->lastName . ' ' . $this->firstName;
    }

    public function exampleFunction(): void
    {
        echo 'Comportement de la méthode ToDo par défaut de Person <br>';
    }
}
```

Prenons maintenant une classe **Former**, qui se base sur **Person** (`extend` donc) sans rien lui ajouter.

```php
class Former extends Person
{

    // Facultatif, car on ne modifie pas le comportement du constructeur de Former
    // Donc par défaut il va chercher celui de Person
    public function __construct(string $lastName, string $firstName)
    {
        parent::__construct($lastName, $firstName);
    }

    public function __toString(): string
    {
        return 'Class Former : ' . parent::__toString();
    }

    public function exampleFunction(): void
    {
        echo 'Comportement de la méthode ToDo redéfinit par Former <br>';
    }
}
```

Prenons maintenant une classe **Student**, qui se base sur **Person** (`extend` toujours) et qui lui ajoute un `$studentNumberCard`.

```php
class Student extends Person
{
    private string $studentNumberCard;

    public function __construct(string $lastName, string $firstName, string $studentNumberCard)
    {
        parent::__construct($lastName, $firstName);
        $this->studentNumberCard = $studentNumberCard;
    }

    public function __toString(): string
    {
        // parent::__toString() affiche "$this->lastName . ' ' . $this->firstName"
        return 'Class Student : ' . parent::__toString() . ' (' . $this->studentNumberCard . ')';
    }

    // Facultatif, car on ne modifie pas le comportement de la méthode toDo de Student
    // Donc par défaut il va chercher celui de Person
    public function exampleFunction(): void
    {
        parent::exampleFunction();
    }
}
```

Affichons un peu tout ça : 

```php
echo '<h3>Affichage de la class Person</h3>' ;
$person = new Person('Citaire', 'Damien');
echo $person . '<br>';


echo '<h3>Affichage de la class Former</h3>' ;
$former = new Former('Tourret', 'Kevin');
echo $former . '<br>';


echo '<h3>Affichage de la class Student</h3>' ;
$student = new Student('Goncalves', 'Théau', 'SCN063');
echo $student . '<br>';
```

?> On affiche chaque class et on a ça en retour :
![](img/phpobj_class-extend.png)

## Interface

Une interface sert à obliger les classes qu'elle implémente à avoir des comportements obligatoires.<br>
Toute les classes qui implémentent une interface ont accés à une definition des ces methodes.<br>
**On ne peux pas instancier une interface**, elle permets d'imposer des méthodes.<br>
La différence avec une classe qui `extend` c'est qu'on ne peux pas l'**instancier**.

On l'ecrit comme ça : 

```php
interface ContactInterface
{
  public function setFirstName($arg);
  public function getFirstName();
  public function setLastName($arg);
  public function getLastName();
}
```

### Implémenter une interface

```php
class Contact implements ContactInterface
{
  public function setFirstName($arg)
  {
    $this->firstName = $arg;
  }

  public function getFirstName($)
  {
    $this->firstName = $arg;
  }

  public function setLastName($arg)
  {
    $this->lastName = $arg;
  }

  public function getLastName()
  {
    $this->lastName = $arg;
  }

}
```

## Traits

Si on doit créer une classe où son contenu ne changera pas et qu'elle contient des attributs bateau *(nom, description par exemple)*, alors on peux créer un **trait** qui regroupe ces deux-là et l'importer dans des catégories par exemple qui auront toujours ces attributs.<br>
Ca evite de tout recopier et de se faire chier.


## NameSpace

Les namespaces aident à organiser un projet.<br>
Ils ne **remplacent pas** les methodes d'importation mais enrichissent les classes et **permettent de les différencier** s'il y en a 2.<br>
Ils servent à enrichir la définition de nos classes.

C'est utile si on a 2 classes du même nom et ça permets de mieux cibler. *ca peux arriver si on importe pleins de truc venant de différents framework par exemple :*

```php
// on importe la classe Hero qui vient de ArenaRpg
use ArenaRpg\Hero;
// on importe la classe Hero qui vient de OtherRpg
use OtherRpg\Hero;
```
?> Ici on a donc 2 classes Hero qui viennent de **2 sources différentes**, il faudra donc utiliser les namespace pour ne pas être dans la merde avec ces 2 classes **Hero**


## AutoLoad

L'autoload permet de **virer les import de fichier**, il les gère automatiquement.
Il chope les noms de classe et s'occupe d'importer les fichiers qui les possèdent directement
