# Creation d'une base de donnée en SQL

## Les différents types de donnée

Avant d'attaquer les créations de BDDs, tables et autres champs, revoyont différents types de données que nous pourrons ajouter à nos champs : 

- **Numeric** (INT, FLOAT, DECIMAL, BOOLEAN...)
- **String** (CHAR, VARCHAR, TEXT, ENUM...)
- **Date** (DATE, DATETIME, TIME...)

`VARCHAR` est limité à **max 255 charactères** *(pour des petites chaînes de charactère)*. Pour du texte plus long on passe par `CHAR` <br>
`ENUM` sert à stocker une valeur connue *(genre **H** ou **F** pour homme / femme)*, ça securise les données. <br>
`DATE` affiche juste la **date**, `DATETIME` rajoute l'**heure**, `TIME` c'est **juste l'heure**.

<br>
<hr>
<center> :muscle: Maintenant on attaque les choses sérieuses :muscle: </center>
<hr>
<br>

## Créer la Base de données

```sql
CREATE DATABASE `test` DEFAULT CHARACTER SET utf8;
```
?> On créée une BDD du nom de **test** <br>

> Les backquotes sont optionelles, on peux ne rien mettre autour du nom de la base. <br>
> L’encodage UTF-8 est archi-conseillé, c'est le plus répandu.


## Créer une Table

*On voit comment gérer la partie colonnes juste après*

```sql
CREATE TABLE `test`.`new_table`(
    -- colonnes --
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;
```
?> On créée une table **new_table** dans la BDD **test**

> InnoDB est le moteur par default ici (et c'est souvent le cas apparement)


## Créer une Colonne

> Cette commande se fait en même temps qu'on créé une table.

```sql
CREATE TABLE `test`.`new_table`(
    `id` INT NOT NULL AUTO_INCREMENT,
    PRIMARY KEY (`id`);
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;
```
?> On créé ici une table **new_table** avec un champ **id** en son sein. <br> Cet id est un **entier**, **non-null** qui s'**auto-incrémente**, il est ensuite défini comme **clé primaire**


## Ajouter une Colonne

```sql
ALTER TABLE `test`.`new_table`
ADD COLUMN `field_1` INT NULL AFTER `id`
```
?> On ajoute un champ **field_1** dans la table existante **new_table**. <br> C'est un **entier**, **null** et il est calé juste après le champs **id**.


## Ajouter une Clé étrangère

```sql
CREATE TABLE `test`.`new_table2` (
    id INT(3) NOT NULL AUTO_INCREMENT,
    libelle VARCHAR(255) NULL DEFAULT NULL,
    toto_id INT(3) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (toto_id) REFERENCES new_table(id)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;
```
?> On créé une nouvelle table **new_table2** dans la BDD **test** <br>
Au sein de new_table2, on ajoute *(entre autres)* une clé etrangère **toto_id** et on la référence. <br>
Dans ce cas notre clé étrangère  pointe vers la colonne **id** de la table **new_table**.

!> Quand on créer une BDD avec des tables qui ont des `FOREIGN KEY`, il faut créer ces tables qui ont des `FOREIGN KEY`en **DERNIER** car elles peuvent référencer d'autres tables qui ne sont pas encore créées

<br>

!> Si on veux mieux visualiser ce qu'on fait quand on a créé nos tablen on peux afficher en **mode graph** les tables et les jointures en passant par l'onglet **déclencheur** dans **PHPMyAdmin**.

<br>
<hr>
<center>:sunglasses: Et on a fait le tour <i>(en gros hein, mais c'est un début)</i> :sunglasses: </center>
<hr>
<br>


## Quelques exemples de création de BDD/Table/Champs en bonus

```sql
CREATE DATABASE `universite` DEFAULT CHARACTER SET utf8;

CREATE TABLE `universite`.`etudiant`(
    id INT NOT NULL AUTO_INCREMENT,
    nom VARCHAR(120) NOT NULL,
    prenom VARCHAR(120) NULL,
    taille INT(3) NULL,
    sexe ENUM('H', 'F') NULL,
    PRIMARY KEY (`id`)
)
```
?> Cette commande créée une BDD **universite** et lui colle une table **etudiant** avec les champs <br>
**• id** *(entier, non null, auto incrémenté)*<br>
**• nom** *(string/chaine de maximum 120 caractères, non null)*<br>
**• prenom** *(string/chaine de maximum 120 caractères, null)*<br>
**• taille** *(entier de 3 chiffres max, null)*<br>
**• sexe** *(string soit 'H' soit 'F', null)*<br>
Elle defini l'**id** comme **clé primaire**.

<br>
<br>

```sql
CREATE DATABASE `universite` DEFAULT CHARACTER SET utf8;

CREATE TABLE `universite`.`etudiant`(
    id INT NOT NULL AUTO_INCREMENT,
    nom VARCHAR(120) NOT NULL,
    prenom VARCHAR(120) NULL,
    taille INT(3) NULL,
    sexe CHAR(1) DEFAULT NULL CHECK (sexe in ('H','F')),
    PRIMARY KEY (`id`)
)
```
?> Cette commande fait la même chose que la commande précédente avec une alternative au `enum` pour le **sexe**. <br>
Ici on utilise un `CHAR` *(maximum un caractère)* avec un `CHECK` pour verifier que c'est bien un 'H' ou un 'F'
