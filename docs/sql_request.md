# Les requêtes

## Présentation rapide

- SELECT
    - FROM
    - WHERE / AND
    - COUNT
- INSERT
- UPDATE
- DELETE
- DROP *(suprime une table complète)*

### Exemples de répartition dans le **CRUD** : 

- CREATE = `INSERT INTO`
- READ = `SELECT`
- UPDATE = `UPDATE`
- DELETE = `DELETE`