# Les dates

On peux appeler la date avec la fonction pré-définie `date( format, timestamp )`.

Cette dernière prends en argument un **format** sous la forme **j** pour **jour**, **m** pour mois, **Y** pour *year* *etc etc (cf la doc en ligne)*.

La date est calculée par le **TimeStamp**.<br>
**TimeStamp** : nombre de secondes ecoulées depuis le *01/10/1970 à 00:00*; On l'utilise comme référence pour caculer le temps.


## Exemples de dates

```php
date_default_timezone_set('Europe/Paris')
```
?> Pour caler l'heure sur le bon fuseau

<br>

```php
echo date('j/m/Y - H:i:s');
```

?> Retourne la date et l'heure actuelle au format **jour / mois / année - heure : minute : seconde**

<br>


```php
echo date("j/m/Y", strtotime("27 June 1988"));
```

?> Retourne : 27/06/1988

<br>

```php
echo date("j/m/Y", strtotime("+6 months 13 days 24 minutes"));
```

?> Retourne la date actuelle **plus** 6 mois 13 jours et 24 minutes

