*Best source d'infos à jour sur le JS : https://developer.mozilla.org/fr/*

## Presentation

Language créé en 1995, dès le début on pouvait l'executer dans un navigateur et même côté serveur *(c'est pas **NodeJS** qui a revolutionné le game)*.

L'organisme qui organise le **JS** est **ECMA Internationnal**.
Les specifications du JS actuellement se nomment ES2019 (**E**cma**S**cript version de 2019) et evoluent d'années en années.

Le JS etait un peu mou jusqu'à 2015 avant de se relancer vigoureusement avec l'ajout des classes par exemple.

C'est un language à **typage faible**, **interprété** *(lu à la volée par le navigateur, on ne le compile pas en executable)*.

> Pour rappel, quand on *compile* *(avec react, typescipt, sass par exemple)* c'est  de la **transpilation**, pas de la compilation, nuance :relieved: 

Javascript est **orienté Objet** : un nombre, une chaine de caractère, un tableau et même nos fonctions sont **tous des objets** qu'on peux les manipuler comme tels.

## La programation événementielle:

Contrairement à PHP ou CSS par exemple où le code est lu dans l'ordre, on a ici une notion d'évenements.<br>
On a du code qui sera executé, mais on ne sait pas **quand**, ça arrive quand un **event** arrive dans le navigateur *(une action sur un bouton par exemple)*.
### Les events

*Prenons cet exemple :*

On créé un bouton avec un `id` dans notre html.<br>
On le cible en js et on ajoute un **ecouteur d'événement** (`addEventListener`) :

```js
// On selectionne le btn :
const $alertBtn = document.querySelector('#alert-btn');

// On ajoute l'ecouteur d'événement, basé sur le click :
$alertBtn.addEventListener('click', () => {
    alert('Bonjour');
});
```

?> Si on clique sur le fameux bouton dans notre page, une alerte sera **déclenchée**.

Le **1er argument** dans `addEventListener` concerne le **type d'événement** qui sera écouté.<br>
Il s'agit des informations par rapport à l'événement.

Ca peux être par exemple des infos comme : 
- la position de l'élément cliqué,
- si la touche ctrl est pressée quand on click,
- si on fait un click droit,
- quand on hover l'élément,
- des événements liés au clavier,
- etc

_ Pour les consulter on peux rajouter un argument `event` et on le `console.log()`.<br>
Quand on cliquera sur le bouton, tout ce qu'on peux avoir comme info s'affichera.

Démo : 

```js
// On ajoute event :
$alertBtn.addEventListener('click', (event) => {

    // on le console.log
    console.log(event);
    alert('Bonjour');
});
```

?> Ici, on utilise le `click` comme **déclencheur**, il est applicable sur **tous les éléments html**.<br>
Il existe plein d'autres déclencheurs, plus spécifiques et valables seulement sur certain éléments html.


_ Sinon la liste complète est ici aussi hein :sunglasses: : https://developer.mozilla.org/fr/docs/Web/Events

> Les informarions remontées par `event` peuvent par exemple être exploitées pour tracker des comportements sur une page web si on le souhaite*


Le **2eme argument**, optionnel, dans `addEventListener` est l'**element concerné par l'evenement**.

#### Exemple d'exploitation des events

```js
// Tracker curseur
const $cat = document.createElement('img');
$cat.src = 'https://pixel.nymag.com/imgs/daily/vulture/2016/09/29/29-grumpy-cat.w190.h190.jpg';
$cat.style.width = '50px';
$cat.style.height = 'auto';
$cat.style.position = 'absolute';

document.body.addEventListener('mousemove', (event) => {
    document.body.appendChild($cat);

    $cat.style.top = event.clientY + 'px';
    $cat.style.left = event.clientX + 'px';
});
```

?> On a une super image de Grumpy cat qui suit notre curseur, partout, tout le temps :relieved: 

## Importer le JS

On charge le js dans notre html avec la balise `<script>` *(en général juste avant le fermeture du `</body>`)*. Cela se fait soit en chargeant un fichier externe *(script.js par exemple)*, soit en le tapant direct dans les balises :

```html
<script>
    console.log('Bonjour');
</script>
```
ou
```html
<script src="scripts.js"></script>
```

On peux verifier que notre fichier js est bien chargé en y insérant par exemple un `console.log('Bonjour');`, le **Bonjour** apparaitra dans la console, sur l'inspecteur du navigateur.

> Si on veux concaténer une variable avec un string dans un `console.log()`, on les sépare simplement par une virgule. <br>
Par exemple  : `console.log('Bonjour', variableUserName);`

?> Les bonnes pratiques voudraient qu'on charge plutôt le fichier en externe, ne serait-ce que si ce code est **partagé dans plusieurs pages** du site ou même aussi parce que le fichier se **mets en cache**, *(du coup moins de requêtes, plus de perfs :muscle:)*.

## L'inspecteur

Sur l'onglet **console** on a les retours qui s'affichent mais on peux aussi directement y taper directement du JS.

On a aussi l'onglet **source** où on peux mettre des **points d'arrêt** *(en cliquant sur les ligne du code dans l'affichage des scripts)* pour faire des pauses dans l'execution du code, et voir comment ça se passe dans son execution.

![](img/js_intro_point-darret.png)

Les points d'arrêt interrompent le programme une fois que l'exécution atteint un certain point. On peux ensuite parcourir le programme ligne par ligne en observant son exécution et en inspectant le contenu de nos variables.

## Les Variables
### Déclarer une variable

On utilise le `let` et le `const` pour les variables.<br>
Il y a aussi `var` mais il est obsolète, on l'**oublie** *(il est out depuis 2015 environs)*

> `let` peut avoir une valeur qui lui est **réaffectée** tandis que `const` a une valeur **fixe**, qui ne bougera jamais.

```js
// Déclarer la variable :
let myDeclaredVar;
// Affectater une valeur à la variable :
myDeclaredVar = 'variable content';

// Déclarer une variable avec initialisation
// (On créer une variable et on lui attribue une valeur directement)
// Reviens à faire les 2 ligne au dessus en une seule
let myVar = 'variable content'; 


// CONST, (initialisation obligatoire)
const MY_CONST = 'variable content'; 
```

> Quand on **déclare** une variable, on dit qu'elle existe *(on utilise `let` dans l'exemple)*. <br> Quand on lui donne sa toute première valeure, on dit qu'on l'**initialise**

#### Quelques exemples :

```js
// Premier caractère autorisé pour le nommage de variable : $ _ a A
// Les autres caractères autorisés pour le nommage de variable : $ _ a A 0

let myInt = 42;
let myFloat = 4.2;
let myString = 'string';
let isValid = true;
let isInvalid = false;
```

Contrairement à PHP, et comme on peux le voir sur l'exemple ci-dessus, le `$` n'est ici pas obligatoire au début du nom des variables.<br>
On peut toutefois garder cette pratique, ce qui facilite la lecture du code.

!> On ne **redéclare** pas une variable une seconde fois, mais on peux lui affecter une **nouvelle valeur** : `myAlreadyDeclaredVar = 'new value'` : 

```js
myInt = 'string';
```

?> On affecte une nouvelle valeur à la variable myInt créée juste au dessus.<br>
## Les conditions

```js
let isValid = true;

if (true === isValid) {
// if (isValid == true) {
// if (true === isValid) {
    consol.log('OK');
} else {
    console.log('KO'),
}
```
?> On aura un log **OK** dans la console car la variable `isValid` est égale à `true`.

Le resultat ne sera pas le même suivant le nombre de `=` que l'on a dans notre `if`.

**Exemples :** 
```js
// Vrai si j'ai comme valeur
// 1
// '1'
// true
// 'true'
// 'nrsaunrisnrauinrs'
// objet
if (isValid == true) {

}

// Vrai si j'ai comme valeur
// 0
// '0'
// false
// 'false'
// ''
// null
if (isInvalid == false) {

}

// Vrai si j'ai comme valeur
// true
if (isValid === true) {

}

// Vrai si j'ai comme valeur
// false
if (isInvalid === false) {

}

if (isInvalid !== false) {
// if (isInvalid != false) {

}
```

## Les opérateurs relationnels

**Quelques exemples avec les opérateurs relationnels :**

```js
if (myInt < '0') {
// myInt inférieur à 0 (0 ecrit en string)

if (myInt < 0) {
// myInt inférieur à 0 (0 ecrit en number)

if (myInt > 0) {
// myInt supérieur à 0

if (myInt >= 0) {
// myInt supérieur ou égal à 0

if (myInt <= 0) {
// myInt inférieur ou égal à 0


if (myInt < 0 && myInt > -50) {
// myInt inferieur à 0 ET myInt supérieur à -50

if (myInt < 0 || myInt > -50) {
// myInt inferieur à 0 OU myInt supérieur à -50



// 1
// 42
// 90
// 500
// -50
if (
    myInt > 42 && (myInt < 100 || myInt < 0)
) {
    // OK => 90, -50

    // OK => 90
}

let result = 5 * 9 + 8;
let result2 = 8 + 5 * 9;
```

> Il y a les même priorités d'opérateurs qu'en maths *(priorité aux multiplications, prends en compte les parenthères...)*. <br> On retrouve le même raisonement avec les `&&` et les `||` *(de base le `&&` est prioritaire sur le `||` mais on peux mettre des parenthèses aussi pour modifier les priorités)*;

### Ternaire

Une condition ternaire a cette forme :

```js
const terVal = isValid ? 42 : 963;
```

## Les tableaux

On déclare un tableau : 

```js
let myArray = []; // un tableau vide
let myArrayWithValue = [1, 2, 34, 5, 98562, 42];
// les indexs :        [0, 1,  2, 3,     4,  5];
```

Si on veux connaitre la longeur de notre tableau :

```js
myArrayWithValue.length;
```

Pour afficher des valeurs dans le tableau on fait :
```js
console.log(myArrayWithValue[3]);
```
?> On **affiche la 4eme entrée** dans le tableau *(on n'oublie pas que le 0 compte)*, <br>
Dans ce cas ce sera **5** *(si on se réfère au tableau déclaré plus haut)*.

!> Si on tape sur un index inexistant *(`console.log(myArrayWithValue[12]);` par exemple)*, on aura **undefined**.

> On a 3 types de retour quand on n'a rien :<br> 
> - `null` : une valeur qui représente le vide <br>
> - `void` : on a aucun retour <br>
> - `undefined` : cette valeur n'as pas été initialisé/definie *(une variable declarée mais pas initialisée est `undefined`)*

### Manipuler un tableau

#### Pour **ajouter** une valeur à un tableau on fait

```js
myArrayWithValue.push(42);
// ajoute un élément (42) à la fin d'un tableau

myArrayWithValue.unshift(42);
// ajoute un élément (42) au début d'un tableau 
// (il décale tous les index du tableau)
```

#### Pour **enlever** une valeur à un tableau on fait

```js
myArrayWithValue.pop();
// retire un élément à la fin d'un tableau (inverse du `push`) et le retourne


myArrayWithValue.shift();
// retire un élément au début d'un tableau  (inverse du `unshift`) et le retourne
// (il décale tous les index du tableau)
```

<br>

> Quand on veut retirer une donnée d'un tableau, on peux la recupérer et la stocker dans une variable par exemple

```js
saveVal = myArrayWithValue.shift();
// Stocke la premiere valeur du tableau *myArrayWithValue* dans la variable saveVal
```

<br>

#### Pour chopper l'**index** d'un élément

```js
// on créé un nouveau tableau
let myArrayWithValue = [1, 2, 34, 5, 98562, 42, 2, 78];
// les indexs :        [0, 1,  2, 3,     4,  5, 6,  7];


myArrayWithValue.indexOf(34);
// Retourne la valeur de l'index de l'élément 32
// On a en retour : 2

myArrayWithValue.lastIndexOf(2);
// Retourne la valeur du dernier index de l'élément 2
// -> on a ici 2 fois l'élément 2 dans le tableau, 
// avec cette commande il nous retournera 6 car c'est le dernier 2 du tableau
```

> Si on tape sur un index qui n'existe pas on a **-1** en retour.

<br>

#### Pour **concaténer** 2 tableaux

```js
let newArray = myArrayWithValue.concat([1254, 4568, 56]);
// Ajoute les valeurs renseignées au tableau myArrayWithValue 
// sous la forme d'un nouveau tableau qui s'appelle newArray
```
> On créé toujours un nouveau tableau dans ce cas de figure

<br>

#### Pour afficher un tableau **à partir d'un index**

```js
myArrayWithValue.slice(5);
// Affiche le contenu de myArrayWithValue à partir de l'index 5 et jusqu'à la fin

myArrayWithValue.slice(2, -2);
// Affiche le contenu de myArrayWithValue à partir de 
// l'index 2 et jusqu'à la fin moins 2 index.
// La 2eme entrée defini un intervale
```

<br>

#### Pour récupérer la lettre affichée à un endroit précisé (un index) dans un string

```js
const sentence = 'The quick brown fox jumps over the lazy dog.';

const index = 3;

console.log(`La lettre à l'index ${index} est ${sentence.charAt(index)}`);
// On aura en retour  "La lettre à l'index 4 est q"
// Si on avait mis l'index à 0 on aurait eu 'T'
// Les espace sont peis en compte
```


## Les boucles

### foreach

Toujours comme pour php, foreach permets de parcourir un tableau.

```js
const list = ['a', 'b', 'c']
list.forEach((item, index) => {
  console.log(item) //value
  console.log(index) //index
})

//index est optionnel ici
list.forEach(item => console.log(item))
```

Attention toutefois, la méthode n’utilise pas une **copie du tableau** lorsqu’elle est appelée, elle **manipule le tableau directement**. Donc si on modifie le tableau en cours de route alors les boucles pourront être impactées.

```js
let mots = ["un", "deux", "trois", "quatre"];

mots.forEach((mot) => {

  console.log(mot);

  if (mot === "deux") {
    mots.shift();
    // rappel, shift enlève une entrée au tableau
  }
});

// On obtient :
// un
// deux
// quatre
```
### for

3 conditions dans le for : 
- **code initial** *(généralement déclaration d'un compteur avec initialisation)*
- **condition pour continuer la boucle**
- **code éxécuté après chaque itération** `n`, avant de verifier si ok pour itération `n + 1`

> Petit rappel: `i++` reviens à faire `i = i + 1`

```js
for ( let i = 0; i < 10; i ++ ) {
    console.log(i);
}
```

?> On a un **i** qui fait **0**, au final on veux qu'il soit **inférieur à 10**, on **incrémente i** jusqu'a ce qu'on arrive juste avant **10, (9)**. <br>
On a donc **chaque etape avec i + 1 de O à 9**

> - Une boucle for peut être interrompue en utilisant l’instruction break
> - On peut passer à l’itération suivante en utilisant le mot clé continue

### for avec des tableaux

```js
for (let key = 0; key < myArrayWithValue.length; key++) {
    const item = myArrayWithValue[key];
    console.log(key, item);
}
// Retourne chaque entrée du tableau myArrayWithValue avec son index
// /!\ Cette version n'est pas encore supporté par tous les navigateurs, on a donc une autre version pour chopper les index si on veut, qui passe par foreach :
foreach ($myArrayWithValue as $key => $item) {
    console.log(key, item);
}
```

`for of` permets de parcourir un tableau :

```js
for (let item of myArrayWithValue) {
    console.log(item)
}
// Retourne chaque entrée du tableau myArrayWithValue
/// -> equivalent avec un foreach :
foreach ($myArrayWithValue as $item) {
    console.log(item)
}
```

### for in

L’instruction `for..in` en javascript permet d’itérer sur l’ensemble des propriétés énumérables d’un objet :

```js
let student = { name:"Bill", age: 25, degree: "Masters" };
for (var item in student) {
   alert(student[item])
}
// résultats :
// Bill
// 25
// Masters
```

> Cette instruction permet également d’itérer sur les tableaux mais elle peut avoir des comportements inattendus. *En effet l’instruction permet d’itérer sur les propriétés ajoutées manuellement en plus des éléments du tableau ce qui pourrait porter à confusion*.

### while

```js
while ( isValid === true ) {
    const rand = Math.random();

    console.log('Boucle 1 : ', rand);

    if (rand < 0.2) {
        isValid = false;
    }
}
```

?> `rand` sort un **float ente 0 et 1**. Tant qu'on a un résultat supérieur à 0.2, il nous log la valeur retournée, il se stopera à la 1ere valeur inférieur à 0.2

### do while

Crée une boucle qui exécute une instruction jusqu'à ce qu'une condition de test ne soit plus vérifiée. La condition est testée après que l'instruction soit exécutée, le bloc d'instructions défini dans la boucle est donc exécuté au moins une fois.

Le `do while` se voit comme ça : 

```js
do
   instruction
while (condition);
```

**instruction**<br>
Une instruction exécutée **au moins une fois** et **ré-exécutée chaque fois que la condition de test est évaluée à `true`**.

**condition**<br>
Une expression évaluée **après chaque passage dans la boucle**.<br>
Si la condition donne `true`, l'**instruction sera exécutée à nouveau**.

<br>

On reprends sensiblement le même code que pour le while juste au dessus.

```js
do {
    const rand = Math.random();

    console.log('Boucle 2 : ', rand);

    if (rand < 0.2) {
        isValid = false;
    }
} while (isValid === true && val === 'bonjour');
```

?> On fait d'abord la 1ere itération et après on se comporte comme un while classique.
### Les fonctions

Anatomie d'une fonction : 

```js
function myFunc(arg1, arg2, arg3) {
    if (true) {
        // qqc
    } else {
        // autre chose

        return false;
    }

    return true;
}
```

Exemple d'addition : 

```js
function addition(nb1, nb2) {
    return nb1 + nb2;
}

console.log(addition(2, 3));
```
### Les fonction anonymes

#### Anatomie 

```js
const myAnonymousFunc = function (arg1, arg2) {
    return arg1 - arg2;
}
```
<br>

```js
myAnonymousFunc(52, 52)
// on appelle une fonction qui fait une soustraction
// elle renverra 52 - 52 donc 0

const anotherNameFunc = myAnonymousFunc;

anotherNameFunc(42, 52)
// on appelle la fonction myAnonymousFunc sous un autre nom
// défini avec la const anotherNameFunc
// elle fait le même taf que la 1ere fonction
// elle renverra 42 - 52 donc -10
```

?> On **stocke une fonction dans une variable** pour la **rééxploiter en argument dans d'autres fonctions**

On a là un **callback**, une fonction de rappel.

<br>

**Exemple d'utilisations avec `reduce`:**

> reduce applique une fonction qui est un « accumulateur » et qui traite chaque valeur d'une liste (de la gauche vers la droite) afin de la réduire à une seule valeur.

```js
// Autres formes :
let myArrayWithValue = [5, 8, 6];

const newArray = myArrayWithValue.map(function (item, key) {
    return item * key;
});
// Multiplie chaque valeur de myArrayWithValue par son index, 
// on a donc :
// (5 * 0) -> 0
// (8 * 1) -> 8
// (6 * 2) -> 12

const reduceAddition = function (prev, item, key, arr) {
    return item + prev;
};
// Addditionne entre-elles chaque valeurs

const total = myArrayWithValue.reduce(reduceAddition, 0);
// Total sera egal à 19 (5 + 8 + 6)
// Il applique la fonction reduceAddition au tableau myArrayWithValue

const total2 = newArray.reduce(reduceAddition, 0);
// Total sera egal à 20 ((5 * 0) + (8 * 1) + (6 * 2))
// Il applique la fonction reduceAddition au tableau newArray généré pus haut
```
### Le callback

!> Divers exemples de cas où on a des callback<br>
*Notes diverses, à approfondir*

Si on a une fonction qui a besoin d'une fonction anonyme, c'est un **callback**.

Quand on utilise les ecouteurs d'event on a des **callback**.

### Map

Construit un **nouveau tableau** pour **chaque index du tableau de référence**.<br>
On récupère chaque entrée d'un tableau, on le parcours en modifiant chaque entrée tout en créant un nouveau tableau.

**Exemple d'une fonction qui recréé ce comportement juste en dessous**

```js

// Prends 2 arguments : le tableau d'entrée et en 2eme le callback (une fonction anonyme ici)
// (ici le callback nous est inconnus pendant l'execution de la fonction mais ce n'est pas grave)

function arrayMap(arr, callback) {
    // On créer un nouveau tableau
    const newArray = [];
    // on parcours le 1er tableau,
    for (const item of arr) {
        // on modifie chaque entrée pour remplir le nouveau tableau
        newArray.push(callback(item));
    }
    // on retourne le nouveau tableau
    return newArray;
}

let myArrayWithValue = [5, 8, 6];

const newArrayWithCustomMap = arrayMap(myArrayWithValue, function (item) {
    return item + 42;
});
```
