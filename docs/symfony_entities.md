## Créer une entité

Pour créer une **entité** *(ou **classe métier**)* on tape la commande suivante :

```bash
php bin/console make:entity
```

Le dialogue suivant va se lancer : 

```bash
Class name of the entity to create or update:
> Product


New property name (press <return> to stop adding fields):
> name


Field type (enter ? to see all types) [string]:
> string


Field length [255]:
> 255


Can this field be null in the database (nullable) (yes/no) [no]:
> no


New property name (press <return> to stop adding fields):
> price


Field type (enter ? to see all types) [string]:
> integer


Can this field be null in the database (nullable) (yes/no) [no]:
> no


New property name (press <return> to stop adding fields):
>
(press enter again to finish)
```

?> On renseigne à chaque étape ce qui va constituer notre entity.<br>
Ici, ce sera une entité avec le nom **Product**.<br>
On a un champs **name** de type **string** avec un length à **255**. Ce champs ne peut **pas** être **NULL**.<br>
On a aussi un champs **price** de type **integer** qui ne peut **pas** être **NULL**.

Une fois qu'on a validé ces étapes, un fichier `Product.php` est généré dans `src/Entity` ainsi qu'un fichier `ProductRepository.php` dans  `src/Repository`.

Le fichier `Product.php` a cette tête :

```php
// src/Entity/Product.php
namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    public function getId(): ?int
    {
        return $this->id;
    }

    // ... getter and setter methods
}
```

?> Ce joli petit fichier comporte toutes les informations qui pourront être envoyées à la BDD :sunglasses:


Une fois que c'est fait et qu'on est ok, on **prevoit la migration** en tapant :

```bash
php bin/console make:migration
```

?>On valide et un fichier `Version-etc.php` sera créé dans le dossier`migrations`.<br>
Ce fichier inclus nottament la requête qui sera envoyée à notre BDD pour ajouter notre entity.

!> Attention, il faut avoir une **BDD** qui **tourne** et qui est **accessible** *(via le fichier `env` par exemple)*

On peux maintenant envoyer tout ça à notre BDD en tapant :

```bash
php bin/console doctrine:migrations:migrate
```

?> La BDD va être **mise à jour** avec la **nouvelle table**.

### Créer des requêtes

On peux maintenant se rendre sur le fichier `ProductRepository.php` et y crééer des fonctions qui renveront des requêtes à notre BDD afin de choper des infos.
Ces fonctions seront appelées dans le Controller associé et enverront tout ça à la vue que l'on désire.


## Doctrine, créer des relations entre les entities

### Version Basique

Doctrine Fait la liaison entre les tables et les entités metier.

https://www.doctrine-project.org/projects/doctrine-orm/en/2.7/reference/association-mapping.html

Pour créer les **relations** il faut d'abord avoir **déjà créé des entités**.

On fait un `make:entity` sur une entité qu'on veux **modifier**, il sait déjà qu'elle existe et propose de la modifier du coup.

On precise ensuite à quelle entité on veux faire la relation, on renseigne donc le **nom de la classe**.

On ajoute ensuite une **relation**, on precise ensuite le type *(comme ca il nous présente en détail chaque relations possibles: ManyToOne, OneToMany, ManyToMany, OneToOne [rarement utilisé le OneToOne])* .

On fini le process, il nous proposent de tout save et c'est fait :thumbsup:

### Version plus complexe

On peux avoir besoin de créer des relations un peu + complexes.<br>
Prenons un exemple avec une liaison entre une entity **Album**, un **Genre** et un **Artiste**.<br>
Là il faut le faire **à la main**.


On commence par notre entité **AlbumArtist** avec maker (`...make:entity` on connait maintenant), on se rapproche des tables intermediaires là.

```bash
entity-name
> AlbumArtist

property-name
> album

field-type
> relation

class-related
> Album

relation-type
> ManyToOne

alowed-to-be-null
> no

add-property-to-target-class
> yes

new-field-inside-target
> albumArtist

delete-orphan-auto
> yes

```

?> Dans **AlbumArtist** on a tout ce qu'il faut qui a été créé.<br>
Dans **Album**, pareil, on a les relations qui sont créées avec le `mappedBy` et le `inversedBy` suivant le champs.

On enchaine avec la liaison suivante :

```bash

entity-name
> AlbumArtist

property-name
> artist

field-type
> relation

class-related
> Artist

relation-type
> ManyToOne

alowed-to-be-null
> no

add-property-to-target-class
> yes

new-field-inside-target
> albumArtist

delete-orphan-auto
> yes

```

?> Une entité *AlbumArtist* est donc créée et se charge des liaisons entre Album et Artist.

!> Pour le ManyToMany il faut à tout prix avoir le inversedBy d'un côté et le de l'autre


## Recap de relations 

On reprends l'entity **AlbumArtist** pour avoir un aperçu et un recap des relations :

```php
/**
 * > Relation ManyToOne bilatérale, c'est à dire :
 * > On peut accéder à nos Album depuis AlbumArtist
 * > On peut accéder aux AlbumArtist depuis Album
 *
 * @ORM\ManyToOne(targetEntity=Album::class, inversedBy="albumArtist")
 * @ORM\JoinColumn(nullable=false)
 */
private $album;

/**
 * > Relation ManyToOne bilatérale, c'est à dire :
 * > On peut accéder à nos Artist depuis AlbumArtist
 * > On peut accéder aux AlbumArtist depuis Artist
 *
 * @ORM\ManyToOne(targetEntity=Artist::class, inversedBy="albumArtists")
 * @ORM\JoinColumn(nullable=false)
 */
private $artist;

/**
 * > Relation ManyToMany unilatérale, c'est à dire :
 * > On peut accéder à nos Genre depuis AlbumArtist
 * > On ne peut pas accéder aux AlbumArtist depuis Genre
 *
 * @ORM\ManyToMany(targetEntity=Genre::class)
 */
private $genres;

// --- etc
```

Dans l'entity **Album**, on a ça : 

```php
/**
 * > Relation ManyToMany bilatérale, c'est à dire :
 * > On peut accéder à nos Song depuis Album
 * > On peut accéder aux Album depuis Song
 *
 * @ORM\ManyToMany(targetEntity=Song::class, inversedBy="album")
 */
private $songs;

/**
 * > Relation OneToMany bilatérale, c'est à dire :
 * > On peut accéder à nos AlbumArtist depuis Album
 * > On peut accéder aux Album depuis AlbumArtist
 *
 * @ORM\OneToMany(targetEntity=AlbumArtist::class, mappedBy="album", orphanRemoval=true)
 */
private $albumArtist;

// --- etc
```

Dans l'entity **Artist**, on a ça : 

```php
/**
 * > Relation OneToMany bilatérale, c'est à dire :
 * > On peut accéder à nos AlbumArtist depuis Artist
 * > On peut accéder aux Artist depuis AlbumArtist
 *
 * @ORM\OneToMany(targetEntity=AlbumArtist::class, mappedBy="artist", orphanRemoval=true)
 */
private $albumArtists;

/**
 * > Relation ManyToOne bilatérale, c'est à dire :
 * > On peut accéder à nos Country depuis Artist ($artist->getCountry())
 * > On ne peut pas accéder aux Artist depuis Country
 *
 * @ORM\ManyToMany(targetEntity=Genre::class)
 */
private $genres;

// --- etc
```

Dans l'entity **Country**, on a ça : 

```php
/**
 * Par défaut Doctrine fait du LAZY => Il ne va chercher les entités en relation que lorsqu'il en a besoin
 * Toutefois cette méthode est plus coûteuse en requêtes (et donc en temps) car il va effecteur les requêtes sur le moment
 * C'est à dire, lorsque vous en avez besoin
 *
 * Il existe le fetch="EAGER" => qui lui intègrera vos entités en relation par défaut, et ce tout le temps
 * Que vous ayez besoin de les réutiliser ou non
 *
 * @ORM\OneToMany(targetEntity=Artist::class, mappedBy="country", fetch="LAZY")
 */
private $artists;

// --- etc
```

Dans l'entity **Song**, on a ça : 

```php
/**
 * > Relation ManyToMany bilatérale, c'est à dire :
 * > On peut accéder à nos Album depuis Song
 * > On peut accéder aux Song depuis Album
 *
 * @ORM\ManyToMany(targetEntity=Album::class, mappedBy="songs")
 */
private $album;

// --- etc
```

?> Dès qu'on a un ManyToMany on a forcément une table de relation qui sera créée. Dans cette table on retrouve un artist_id et un genre_id par exemple (mais pas d'ID tout cours).
#### Relations association :

![](img/symfony_relation_association.png)

#### Relations many to many :

![](img/symfony_relation_manyToMany.png)

### fetch EAGER et LAZY


Dans mes requêtes : `fetch=" "` est soit `EAGER` soit `LAZY` :

`EAGER` renvoie **plus d'info** mais la requête est **plus lourde**, **plus longue**. Il fait un `JOIN` ne même temps.<br>
`LAZY` est **plus light** donc **plus rapide** mais renvoie **moins d'infos**.

![](img/symfony_relation_fetch.png)
