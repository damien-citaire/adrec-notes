# Php examen

*Informations vues pendant la préparation à l'examen de mi-décembre 2020*

## Secu BDD PDO

Pour éviter d'envoyer les information de la BDD du projet en clair sur le git, on peux créer un fichier **config.php** par exemple qui contiendra ces infos et qui sera appelé dans le projet.

Ce fichier stocke les variables en local et il est envoyé une première fois sur le serveur distant avec aucune info sous le nom **config-sample.php**:

```php
//Database configuration
const DB_HOST = 'xxxx';
const DB_NAME = 'xxxx';
const DB_USER = 'xxxx';
const DB_PASSWD = 'xxxx';
```

Seul le fichier **config-sample.php** est dispo dans le projet git en ligne. Il pourra être recupéré et dupliqué sous le nom **config.php** et rempli avec les infos de la BDD en local. 

Voilà les infos de la BDD sont protégées.

#### Petit rappel Git

Tant qu'on est sur Git, un dossier **vide** ne sera pas envoyé en ligne. Si on le souhaite tout de même on peux placer un fichier `.gitkeep` vide. Il permets que le dossier ait une existence en ligne et soit donc envoyé au commit, même s'il ne contient rien d'autre.


## Composer

Composer est un logiciel gestionnaire de dépendances pour PHP.

```bash
composer init
```

?> Permets de générer le fichier composer.json qui défini le projet et repertorie les dépendances.

### Packages sympas pour composer

#### - ramsey/uuid
https://github.com/ramsey/uuid

#### - lip imagine
https://github.com/liip/LiipImagineBundle <br>
*gère la taille des images pour Symfony*


#### - fly system 
https://flysystem.thephpleague.com/v2/docs/ <br>
*(cdn)*

#### - symfony var-dumper
https://symfony.com/doc/current/components/var_dumper.html <br>
*Pour avoir un dumper propre*

#### - ext-pdo
*Pour communiquer avec la BDD*

#### - cocur/slugify
https://github.com/cocur/slugify <br>
*Plus d'info juste en dessous :

## Slug

Un Slug *(ou Friendly URL)* est la partie d’une URL qui identifie le titre d’un article, un blog ou d’une news. <br>
Tout ce qui est caractères spéciaux et accents sont supprimés/remplacés pour rendre le lien lisible et interprétable par quiconque *(humain, navigateur, moteur de recherche, visiteur étranger…)*.

Exemple : <br>
Les chaussettes de l’archiduchesse sont-elles sèches? Archi-sèches ! pour le site http://www.example.com

On peut encoder la chaine avec urlencode() et obtenir le résultat suivant :
http://www.example.com/Les+chaussettes+de+l%27archiduchesse+sont-elles+s%C3%A8ches%3F+Archi-s%C3%A8ches+%21
Mais ce n’est pas très lisible.

Cet exemple ci-contre est en revanche plus convivial : http://www.example.com/les-chaussettes-de-l-archiduchesse-sont-elles-seches-archi-seches

> Petite lib qui fait le taf : 
https://github.com/cocur/slugify


## Fixtures

Permet de créer des fausses données dans les tables pour gérer des relations et donne une bdd exploitable

## Divers

- Quand on utilise une variable qui est une constante on l'ecrit en **majuscule**.

- Si on mets 3 fois egal (=) on verfifie à la fois le resultat mais aussi le **type**.
On mets en général un triple egal pour être sûr de la correspondance (*le 3eme verifie si le type est le même*).


