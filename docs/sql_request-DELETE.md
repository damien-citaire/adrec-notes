## Suppressions dans une table

Pour **suprimer des données** dans les tables on peux être ammené à taper une des commandes suivantes :

!> Les commandes siuvantes :`DROP`, `DELETE` et `truncate` sont des actions **irreversibles**

## Les commandes

### Supprimer des champs *DELETE*

```sql
DELETE FROM voiture
WHERE id = 2
```

?> On supprime la voiture avec l' **ID 2** de la table voiture

> Si on veux delete des champs. En général on tape sur l'**ID**, ça limite les conneries.

<br>

### Supprimer une table, une BDD *DROP*

Pour supprimer une table : 

```sql
DROP table modele;
```

?> On supprime la table **modele**

<br>

Pour supprimer une BDD : 

```sql
DROP DATABASE ma_base;
```

?> On supprime la BDD **ma_base**

> Si on supprime une BDD qui n'existe pas on a une erreur, on peux utiliser alors `IF EXISTS` :

```sql
DROP DATABASE IF EXISTS ma_base
```

<br>

### Vider une table *TRUNCATE*


```sql
TRUNCATE TABLE voiture;
```

?> On supprime **tous les champs de la table voiture**

> `TRUNCATE` ne se fait pas en **noSQL**, il faut utiliser `DELETE FROM` à la place.

