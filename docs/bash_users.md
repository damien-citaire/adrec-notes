# Gestion des utilisateurs

Cette commande permets d'ajouter un utilisateur. 

```bash
useradd -m -s /bin/bash user2
```

`-m` = créé un dossier **home** pour le nouvel utilisateur <br>
`-s /bin/bash` = définit le **shell** du nouvel utilisateur, *ici bash* <br>
`user2` = le nom du nouvel utilisateur, *ici user2*

> Quand on créer un nouvel utilisateur, un groupe associé du même nom est automatiquement créé également.

Quand on affiche la liste de tous les utilisateurs *(rappel, c'est dans /etc/passwd)*, on a pleins d'infos, notamment le **shell** utilisé (listé à la fin), on trouve également le chemin de son dossier **home** parmis ces infos.

On peux **supprimer** des utilisateurs avec `userdel` ou les **modifier** avec `usermod`.