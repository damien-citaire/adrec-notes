## Création d'un script Shell

On commence par créer le fichier de script: `nano monfichier.sh`

!> Quand on créé un fichier, on n'a pas les droits d'exécution par défaut, il faut bien penser à se les rajouter avec le petit `chmod u+x monfichier.sh` (ou alors `a+x` si on veux donner les droits à tout le monde).

## Les Variables

### Présentation

Dans un fichier de script, on peux avoir à utiliser des **variables** : 

**Principe de base :** si, dans un script, on cale la variable `$1`, alors le **1er argument** de la commande sera integré dans son execution : 

- Mon script : <br>
`echo "bonjour" $1`

- Ma commande pour accéder au script *(avec l'argument `toto`)* : <br>
`./script.sh toto`

- Ce qui sera retourné à l'execution du script : <br>
`Bonjour toto`


On peux aller plus loin qu'un simple mot dans notre commande grâce aux doubles quotes : <br>
`./script.sh "tout le monde"` &rarr; `Bonjour tout le monde`

Pour les quotes dans les doubles quotes ou inverse on peux faire : <br>
`"J'aime le chocolat"` ou `'J"aime le chocolat'` ou encore `'J\'aime le chocolat'`.

<br>

On peux aussi aller plus loin et integrer une **commande** en **variable** dans un script, *syntaxe: `$(whoami)`*

##### Exemple

```bash
#! /bin/bash
# on declare que le SHELL sera en Bash

VAR1='Bonjour'
VAR2=`whoami`
VAR3=$(whoami)
VAR4=42
# on declare quelques variables...

_________

echo "Bonjour" $VAR1 $VAR2 $VAR3 $VAR4
# notre script intégrant les variables déclarées ci-dessus
```

### Liste de variables

Variable | Description 
------------ | ------------- 
`$?` | Nous donne le resultat de la commande précédente<br> *0 veux dire pas de soucis, les chiffres varient ensuite selon les erreurs.*
`$#` | Nous renvoie le nombre d'arguments présents dans la commande précédente.
`$1` | Comme déjà-vu : récupère le 1er argument de la commande précédente <br> *(`$2` prends le 2eme argument, etc etc)*
`$@` | Récupère le tableau complet des arguments de la commande précédente en une fois.
`$0` | Reprends la dernière commande tapée.

## Les Conditions

### Présentation

Exemple d'un script jouant avec les conditions en s'appuyant sur certaines variables citées plus haut (`$#` et `$@`): 

```bash
#! /bin/bash

echo -n "Bonjour "

if [[ $# -eq 0 ]]; then
	whoami
else
	echo $@
fi
```

En gros ça veux dire : *Si  la commande précédente n'as pas d'agument, alors je recupère le contenu de `whoami`, sinon je rajoute l'ensemble des arguments posés dans la commande qui vient d'être tapée.*



