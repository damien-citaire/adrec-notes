# Authentification

## Création d'un User

On commence par créer une classe **User** avec la commande suivante :

```bash
php bin/console make:user
```
Le dialogue suivant apparaitra : 

```bash
The name of the security user class (e.g. User) [User]:
> User

Do you want to store user data in the database (via Doctrine)? (yes/no) [yes]:
> yes

Enter a property name that will be the unique "display" name for the user (e.g. email, username, uuid) [email]:
> email

Does this app need to hash/check user passwords? (yes/no) [yes]:
> yes
```


?> Le user est créé, maintenant si on veux ajouter des champs au User, on passe par le `make:entity`.

> Il vaut mieux commencer par créer un User avec le `make:user` puis enchainer avec le `make:entity` pour lui rajouter des champs.


Quand on est okay avec notre nouvelle classe User, on fait ensuite un `php bin/console make:migration` et un `php bin/console doctrine:migrations:migrate`.

## Ajout de l'authentification

On commencer par cette commande : 

```bash
symfony console make:auth
```

Le dialogue suivant apparait : 

```bash
What style of authentication do you want? [Empty authenticator]:
    [0] Empty authenticator
    [1] Login form authenticator
> 1

The class name of the authenticator to create (e.g. AppCustomAuthenticator):
> UserAuthenticator (on mets ce qui nous vas là hein)

Choose a name for the controller class (e.g. SecurityController) [SecurityController]:
> SecurityController

Do you want to generate a '/logout' URL? (yes/no) [yes]:
> yes
```

On a maintenant :
- un **fichier** `src/security/UserAuthhenticator.php`, 
- un **controller** `src/Controller/SecurityController.php` qui est associé à
- une **nouvelle vue**  `templates/security/login.html.twig` qui contient un formulaire d'authentification tout prêt,
- le **fichier** `config/packages/security.yaml` est mis à jour lui aussi.

### Gestion des "sections" accessibles

Dans le fichier `security.yaml`, on a à la fin dans `access_control` la possibilité de **gérer les "sections"** du site qui seront **accessibles** suivant **le rôle** auquel est **associé l'utilisateur**.

*Par exemple, les path commencants par `admin/` pouront logiquement être associés aux users avec le `["ROLE_ADMIN"]`.*

> On peux aussi faire ça dans le controller, mais c'est plus simple ici

## Création d'un Admin

### Hasher un mot de passe

Avant d'**ajouter notre nouvel utilisateur Admin**, on va **hasher un mot de passe** pour ce dernier.

On passe par la commande :

```bash
php bin/console security:encode-password
```

?> On tape ensuite le mot de passe qu'on utilisera, on valide et la version hashée nous sera retournée.

### Ajout à la main dans le SGBD

On va maintenant aller créer un **User** qui aura le **rôle ADMIN** avec son **mot de passe** etc, le tout **directement dans phpMyAdmin**.

- Pour le **mail** on mets celui qu'on veux *(au pif `admin@admin.com` pour l'exemple)*

- Ppour le **rôle** on mets `["ROLE_ADMIN"]`.

- Pour le **mot de passe**, on colle **le resultat hashé** qui nous a été envoyé après notre `php bin/console security:encode-password`.

- On renseigne les éventuels champs potentiellement non-nullables supplémentaire

- On valide et on a notre nouvel **utilisateur Admin**

## Formulaire d'enregistrement

On va créer un formulaire d'enregistrement, on commence par taper cette commande : 

```bash
symfony console make:registration-form
```

Le dialogue suivant s'ouvre :

```bash
Do you want to add a @UniqueEntity validation annotation on your User class to make sure duplicate accounts aren t created? (yes/no) [yes]:
> yes

Do you want to send an email to verify the user email address after registration? (yes/no) [yes]:
> no (on verra une autre fois pour les mails hein)

Do you want to automatically authenticate the user after registration? (yes/no) [yes]:
> yes
```

- `src/entity/User.php` va être adaptée pour s'assurer qu'il n'y a pas de **doublons** avec le mail.

- Un fichier `src/Controller/RegistrationController.php` est créé ainsi qu'un `src/Form/RegistrationFormType.php` et enfin la vue dans `templates/registration/register.html.twig`.

## CRUD des Users

On enchaine avec la **création du CRUD** pour les User :

```bash
symfony console make:crud User
```

?> Le CRUD est généré pour notre class USER

!> Quand on genère un CRUD avec cette commande, le formulaire généré est par defaut **archi basique**. Il va donc falloir revenir dessus.

### Adapter le formulaire par defaut

Comme précisé à l'instant, quand on genère le crud pour les USER, le fichier `src/Form/UserType.php` est **archi basique**.

- Pour **les roles** par exemple, on s'attends à avoir un **array** pour **selectionner les rôles prédefinis** alors que par defaut notre champs s'attends pour l'instant à un simple **string**.<br>
- On a également un champs **text basique** pour le **mot de passe** alors qu'on voudrait plutôt un champs de type **password**.


Il a, dans sa version de base, cette apparence dans notre fichier `src/Form/UserType.php`: 

```php
public function buildForm(FormBuilderInterface $builder, array $options)
{
    $builder
        ->add('email')
        ->add('roles')
        ->add('password')
    ;
}
```

?> Ici le builder nous fournira un formulaire avec un **simple champs text** pour `email`, `roles` et `password`.

On va devoir le **modifier** pour avoir un **select** qui permettra de **selectionner un des rôles predefinis** et un champs de **type password** pour le **mot de passe**.

On obtiendra une formulaire qui aura plutôt cette forme : 

```php
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

// ...
// ...

public function buildForm(FormBuilderInterface $builder, array $options)
{
    $builder
        ->add('email')
        ->add('roles', ChoiceType::class, array(
                'choices' =>
                    array
                    (
                        'ROLE_ADMIN' => array
                        (
                            'Yes' => 'ROLE_ADMIN',
                        ),
                        'ROLE_USER' => array
                        (
                            'Yes' => 'ROLE_USER',
                        ),
                    )
            ,
                'multiple' => true,
                'required' => true,
            )
        )
        ->add('password', PasswordType::class);
    ;
}
```

?> On a maintenant un select pour les rôle ainsi qu'un champ de type password pour le mot de passe.


### Gestion du hashage du password dans le CRUD

Dans le CRUD des Users, le **mot de passe n'est pas hashé par défaut**, alors qu'il le faut bien evidemment.


Direction le **Controller de la class User** pour modifier les formulaire d'**ajout** et d'**edition** de nos **Users** : 

```php
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

// ...
// ...

/**
* @Route("/new", name="user_new", methods={"GET","POST"})
*/
public function new(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
{
    $user = new User();
    $form = $this->createForm(UserType::class, $user);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {


        // 
        // On rajoute ici la gestion du mot de passe :
        // ------
        $user->setPassword(
            $passwordEncoder->encodePassword(
                $user,
                $form->get('password')->getData()
            )
        );


        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        return $this->redirectToRoute('user_index');
    }

    return $this->render('Back/crud/user/new.html.twig', [
        'user' => $user,
        'form' => $form->createView(),
    ]);
}

// --
// --

/**
 * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
 */
public function edit(Request $request, User $user, UserPasswordEncoderInterface $passwordEncoder): Response
{
    $form = $this->createForm(UserType::class, $user);
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {


        // 
        // On rajoute ici la gestion du mot de passe :
        // ------
        $user->setPassword(
            $passwordEncoder->encodePassword(
                $user,
                $form->get('password')->getData()
            )
        );

       
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('user_index');
    }

    return $this->render('user/edit.html.twig', [
        'user' => $user,
        'form' => $form->createView(),
    ]);
}
```

?> On rajoute le `UserPasswordEncoderInterface` et **avant le flush** on **encode le password**. Comme ça le **password envoyé est d'abord hashé**. 

## Created At avec les EventListener

### Création du champ createdAt

On commence par ajouter le champs `createdAt` à notre entité :

```bash
php bin/console make:entity
```

User existe déjà, du coup on le modifie.

```
Class name of the entity to create or update (e.g. GrumpyGnome):
> User

Your entity already exists! So let's add some new fields!

New property name (press <return> to stop adding fields):
> createdAt

Field type (enter ? to see all types) [datetime]:
> datetime

Can this field be null in the database (nullable) (yes/no) [no]:
> no
```

On enchaine avec la migration :

```bash
php bin/console make:migration

php bin/console doctrine:migrations:migrate
```

### Ajout de l'EventListener

Par defaut, le champs `createdAt` n'as pas besoin d'être renseigné dans le formulaire de création d'un `User`: on chope le `datetime` au moment où le formulaire est soumis et c'est bien suffisant.<br>
Par contre, vu que ce champs est **non-nullable** il faut s'assurer que le datetime soit bien ajouté au moment où le form est soumis, on va faire ça avec un `EventListener`, plus précisement avec un `EventSubscriber`.

On créé un dossier `src/EventListener` qui sera reconnu par Symfo. Dans ce dossier, on créer une class `UserSubscriber`. Ce dernier watchera quand un `User` est créé et il va lui rajouter un `createdAt` à la date du moment où le formulaire est soumis.

Le fichier `UserSubscriber.php` à cette tête :

```php
<?php

namespace App\EventListener;

use App\Entity\User;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;

class UserSubscriber implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
        ];
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if($entity instanceof User) {
            $entity->setCreatedAt(new \DateTime());
        }
    }
}
```

Il nous reste enfin à nous rendre dans le fichier `config/services.yaml` et à ajouter :

```yaml
App\EventListener\UserSubscriber:
        tags:
            - { name: doctrine.event_listener, event: prePersist }
```

On a tout ce qu'il nous faut: le `createdAt` sera bien remplis :sunglasses:

### Afficher le createdAt

Si on veux afficher le createdAt dans les vues de nos User (`templates/user/`), on peux ajouter : `{{ user.createdAt|format_datetime(locale='fr') }}`

!> Attention, pour utiliser le filtre `format_datetime` il faut ajouter une extension à twig via `composer require twig/intl-extra`


## Creation de fixtures pour les Users

On peux se créer un premier User avec un rôle admin via les fixtures :

!> On fait ca en **dev**, **pas en prod** hein *(coucou le mot de passe en clair pour un ["ROLE_ADMIN"])*

Dans le dossier `fixtures` on créé un fichier `User.yaml`.

```yaml
APP\Entity\User:
    user0:
        email: 'admin@test.com'
        password: 'coucou'
        roles: ["ROLE_USER", "ROLE_ADMIN"]
```

On peux ensuite taper la commande suivante pour purger la base et charger les fixtures : 

```bash
symfony console hautelook:fixtures:load --purge-with-truncate -n
```