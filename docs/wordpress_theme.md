# Créer un theme perso

https://capitainewp.io/formations/developper-theme-wordpress

`wp-content/theme`, on commence par créer un dossier avec les fichiers suivants :
- `style.css`,
- `functions.php`,
- `index.php`,
- `header.php`,
- `footer.php`

<br>

Dans le `functions.php`, on va commencer par mettre : 

```php
// Ajouter la prise en charge des images mises en avant
add_theme_support( 'post-thumbnails' );

// Ajouter automatiquement le titre du site dans l'en-tête du site
add_theme_support( 'title-tag' );
```

<br>

Dans le `header.php`, on va commencer par mettre : 

```php
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <header class="header">
        <a href="<?php echo home_url( '/' ); ?>">
            // On choppe ici une image pour notre header
            <img src="<?php echo get_template_directory_uri(); ?>/img/logo.svg" alt="Logo">
        </a>  
    </header>
    
    <?php wp_body_open(); ?>
```

<br>

Dans le `footer.php`, on va commencer par mettre : 

```php
<?php wp_footer(); ?>
</body>
</html>
```

<br>

Dans le `index.php`, on va commencer par mettre : 

```php
// On chope le header.php
<?php get_header(); ?>

<h1>Coucou !</h1>

// On chope le footer.php
<?php get_footer(); ?>
```


On créé aussi un dossier `img` à la racine du theme pour choper les images.


## Style

Pour que le style soit pris en compte, on va dans `functions.php` et on ajoute:

```php
// Déclarer style.css à la racine du thème
function wpm_enqueue_styles(){
    wp_enqueue_style(
        // Ici on mets le nom du theme
        'theme-perso',
        get_stylesheet_uri(),
        array(),
        '1.0'
    );
}
add_action( 'wp_enqueue_scripts', 'wpm_enqueue_styles');
```

## Menus

Pour créer des menus, on les declares d'abords dans `functions.php` :

```php
// Ajout du menu
register_nav_menus( array(
	'main' => 'Menu Principal',
	'footer' => 'Bas de page',
) );
```

Il faut ensuite charger les menus déclarés là où on veux dans notre page.
Exemple pour le menu `main` dans le `header` :

```php
<nav class="header-menu">
    <?php wp_nav_menu( array( 'theme_location' => 'main' ) ); ?>
</nav>
```

?> On ajoute donc ici notre menu `main` au sein de notre header *(là où on à placé cette ligne)*.

Ces menus seront ensuite **administrables** dans l'admin de WP.

## Advanced Custom Field

Permets d'ajouter des champs customs dans les pages.