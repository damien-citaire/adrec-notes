## Installer un programme

Pour installer un programme sur Debian on passe par `apt`

##### Exemples *(avec vim)* 

`apt-get install vim` <br>
Pour instaler vim

`apt-get install -y vim` <br>
Installe vim et répond automatiquement **yes** si une question est posé au cours de l'instalation

`apt-get update` <br>
Mets à jour la liste des programmes disponibles pour Debian <br>
La liste des sources disponibles est accessible ici : `/etc/apt/sources.list`,<br>
*Ce fichier se mets à jours qà chaque fois qu'on fait cette commande*

`apt-get remove vim` <br>
Désinstalle vim ou le programe visé

`apt-get install programme1 programme2` <br>
Pour instaler plusieurs programmes en même temps

`apt-cache show vim` <br>
Affiche toutes les infos sur le package vim. *Gaffe à être précis sur le nom du package.*

`apt-cache search vim` <br>
Liste tous les packages en lien avec vim

## L'affichage des resultats

Quand on cherche la liste tous les packages, la quantité de resultats peut être assez énorme.  On a différentes options qui peuvent faciliter la vie.

> On peux cumuler ces options si on veux, par ex: `| grep | sort | less`

`| less` <br>
Permets de **naviguer dans la liste avec les flèches**, *bien utile si on est dans une VM sans possibilité de scroller*

`| sort` <br>
Pour lister les resultats dans l'**ordre alphabétique**.

`| grep lib` <br>
Pour **filter les résultats**, *en tapant cette commande on aura seulement les resultats qui contiennent lib*.

- `grep` peux tout aussi bien être utilisé avec un `ls` par exemple.

`apt-cache search vim > file`<br>
Permets de copier l'ensemble des resultats dans un fichier, *ici, l'ensemble des résultats de la recherche sur **vim** sera copié dans le fichier **file***

`nomdupackage -v`<br>
Pour connaitre la version du package ciblé installé sur notre machine.

`wich nomdupackage` <br>
nous donne le chemin où est installé le package.

`/marecherche` <br>
Permets de **surligner** un ou des termes dans une liste de resultats *(un peu comme un search)* 

##### Exemple pour le surlignage :

`/7.3`

<pre>
<code class="lang-bash" style="padding: 0">
php <span style="background-color: rgba(255, 204, 0, .25)">/7.3/</span>  - blblblblblblbl
php <span style="background-color: rgba(255, 204, 0, .25)">/7.3/</span> bim - blblblblblblbl
php <span style="background-color: rgba(255, 204, 0, .25)">/7.3/</span> bam - blblblblblblbl
php-boom - blblblb <span style="background-color: rgba(255, 204, 0, .25)">/7.3/</span> blblbl
</code>
</pre>

