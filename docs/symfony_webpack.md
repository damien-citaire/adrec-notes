# Webpack

Webpack est un **package qui gère toutes les dépendances d'un projet** *(js, css etc)* en **un seul import on chope toutes les libs css et js** qu'on veux.

## Instalation (pour symfo)

```bash
composer require symfony/webpack-encore-bundle
```

!> Gaffe à bien avoir déjà **Node.js** et **Yarn** sur son poste

Il va sûrement ensuite falloir faire un  : 

```bash
yarn install
```

?> Logique car webpack passe par **Node.js**

<br>

On a entre autre un fichier `webpack.config.js` qui a été créé, ce sera le **fichier central de webpack**.

## Gestion des styles

### Bootstrap

On installe **Bootstrap** avec **yarn** pour y avoir accès :

```bash
yarn add bootstrap
```

### Sass-loader

Dans `webpack.config.js`, on active la ligne où il est question du sass loader :

```js
// enables Sass/SCSS support
.enableSassLoader()
```

?> Maintenant qu'on ne gère plus le style comme des animaux et qu'on utilise Sass il faudra ajouter **sass-loader** avec **yarn**. On passe par cette commande pour ajouter **sass-loader** : 

```bash
yarn add sass-loader@^9.0.1 node-sass --dev
```
### Arborescence pour les styles

Aperçu de l'**arborescence** de dossiers et de fichiers pour notre dossier `assets/styles` : 

```text
.
└── admin
    ├── general.scss
    └── etc ...
└── base
    ├── general.scss
    ├── text.scss
    └── etc ...
└── components
    └── cards
        ├── default-card.scss
        └── etc ...
    └── etc ...
└── libs
    ├── bootstrap.scss
    └── font-awesome.scss
    └── etc ...
└── _variables.scss
└── index.scss
└── libs.scss

```

#### - admin

Ce sous dossier contient tout le style specifique à la **partie admin** de notre site

#### - base

Ce sous dossier contient tout le style **basique** de notre site

#### - components

Ce sous dossier contient dans des sous-dossiers le style **specifique à chaque composants** de notre site *(cards, navbars, alerts, etc ...)*

#### - libs

Ce sous dossier contient tout les imports et le style **specifique à nos librairies** de notre site, séparé dans un fichier par librairie.

Exemple avec **Bootstrap** :

```css
  // Configuration
@import "~bootstrap/scss/functions";
@import "~bootstrap/scss/variables";
@import "~bootstrap/scss/mixins";
@import "~bootstrap/scss/utilities";

// Layout & components
@import "~bootstrap/scss/root";
@import "~bootstrap/scss/reboot";
@import "~bootstrap/scss/type";
/* @import "~bootstrap/scss/images"; */
@import "~bootstrap/scss/containers";
@import "~bootstrap/scss/grid";
@import "~bootstrap/scss/tables";
/* @import "~bootstrap/scss/forms"; */
@import "~bootstrap/scss/buttons";
/* @import "~bootstrap/scss/transitions"; */
@import "~bootstrap/scss/dropdown";
/*@import "~bootstrap/scss/button-group";*/
@import "~bootstrap/scss/nav";
@import "~bootstrap/scss/navbar";

etc....
```

?> Ici on récupère touts les imports par defauts de Bootstrap et, par soucis d'optimisation, on commente les lignes des fichiers dont on sait que le style relatif ne nous interresera pas.

#### - _variables.scss

Ce fichier contient toutes les **variables scss** de notre site.<br>
On y stocke par exemple des variables de couleur, pour les fonts et autres paths.

On peux même  importer des varailbes directement de bootstrap pour le re-transpilation selon notre charte graphique.

```scss
$main-color: #551377;
$secondary-color: #f3f3f3;

$title-font: 'Poppins';
$font-path: '../../fonts';

// bootstrap color
$primary : #69a256;
$secondary:#dc3545;
$info: darken($primary, 10%);
$success: #008000;
$white: #ffffff;
$black: #000;
$danger: #c90303;
$light: #F8F9FA;
$gray: #808080;

$theme-colors: (
    "primary": $primary,
    "secondary":  $secondary,
    "success":    $success,
    "info":       $info,
    "danger":     $danger,
    "light":      $light,
);
```

#### - index.scss

Ce fichier contient touts les **imports** de notre style.<br>
On commence par importer :
- le fichier de `variables.scss` puis,
- on recupère certaines parties importantes depuis Bootstrap *(les fonctions/variables/mixins/utilities...)*, 
- on termine par nos propres styles *(base/admin/components)*.

Avec l'exemple d'arborescence présentée plus haut notre fichier `index.scss` devrait ressembler en gros à ça :

```css
@import "variables";

@import "~bootstrap/scss/functions";
@import "~bootstrap/scss/variables";
@import "~bootstrap/scss/mixins";
@import "~bootstrap/scss/utilities";

//base
@import "base/general";
@import "base/text";

//components
@import "component/card/default-card";

//admin
@import "admin/general";
```

#### - libs.scss

Ce fichier contient touts les **fichiers gérants les imports venant de libraires externes**.<br>
Il a cette tête en gros avec notre arborescence actuelle : 

```css
@import "variables";

@import "libs/bootstrap";
@import "libs/font-awesome";
```

!> Bien penser à importer nos variables ici aussi si on veux custom prooprement nos librairies.


### addStyleEntry pour les styles

#### Ajout dans Webpack.config.js

Direction `webpack.config.js`, on va ici ajouter les `addStyleEntry` pour s'adapter à l'arborescence de nos fichiers, nottament car on va ensuite séparer le lib du reste du style pour gagner du temps de transpilation. Du coup :

- On ajoute donc un **alias** vers le fichier scss `index.scss` qui regroupe **tous nos propres imports SCSS**.

```js
.addStyleEntry('style', './assets/styles/index.scss')
```


- On ajoute aussi notre **alias** vers le fichier scss `lib.scss` qui regroupe **tous les imports SCSS vers des librairies**

```js
.addStyleEntry('libs', './assets/styles/libs.scss')
```

#### Ajout dans les vues

Direction nos vues dns `templates`, on modifie le code existant dans notre fichier `base.html.twig` pour avoir au niveau des styles :

```html
{% block stylesheets %}
    {{ encore_entry_link_tags('libs') }}
    {{ encore_entry_link_tags('style') }}
{% endblock %}
```

?> On charge maintenant nos 2 fichiers de style

## Gestion des scripts

### Typescript

On créé un dossier `scripts` dans `assets` dans lequel on créé un fichier `index.ts` *(si on veux bosser en typescript)*.

De la même manière que pour sass-loader, on va ensuite installer typescript et son loader `ts-loader` :

```bash
yarn add typescript ts-loader@^8.0.1 --dev
```

Il faut ensuite ajouter un fichier `tsconfig.json` à la racine de notre projet avec ça à l'intérieur : 

```
{
    "exclude": [
        "node_modules",
        "vendor"
    ],
    "compilerOptions": {
        "sourceMap": true,
        "downlevelIteration": true,
        "module": "esnext",
        "moduleResolution": "node",
        "target": "es2015",
        "typeRoots": [
            "node_modules/@types"
        ],
        "lib": [
            "es2018",
            "dom",
            "dom.iterable"
        ]
    }
}
```

On se rends dans `webpack.config.js` et on y active la ligne dediée à TypeScript : 

```js
// uncomment if you use TypeScript
.enableTypeScriptLoader()
```

### addEntry pour les scripts

On reste dans `webpack.config.js`  et :

- On ajoute un `addEntry` vers notre fichier `index.ts` **à la place** du `addEntry` du fichier js `app.js` déjà présent.

```js
.addEntry('script', './assets/scripts/index.ts')
```

On va pouvoir importer le JS de Bootstrap via notre nouveau fichier `index.ts` : 

```ts
import 'bootstrap';
```

!> **Bootstrap**, s'il est en version 5, à besoin de `popperjs` pour tourner *(et de jquery si on est encore en v4)*.<br>
Il faut bien penser à ajouter ces dépendance supplémentaires via yarn si besoin (`yarn add @popperjs/core` par exemple).

#### Ajout dans les vues

Direction nos vues dans `templates`, on modifie le code existant dans notre fichier `base.html.twig` pour avoir au niveau des scripts :

```html
{% block javascripts %}
    {{ encore_entry_script_tags('script') }}
{% endblock %}
```

?> Dans un monde parfait on décale ce bloc juste avant la fin du `</body>` car c'est l'endroit conseillé en terme de perf' pour y charger le JS.<br>
On charge maintenant nos scripts de bootstrap et on peux y ajouter ce qu'on veux par la suite.

## Gerer les images

On stockes nos images dans un dossier `images` dans `assets` et elles seront copiées dans public + tard via  `webpack.config.js`. Pour faire la copie on rajoute un `copyFiles` dans ce dernier :

```js
.copyFiles({
    from: './assets/images',
    to: 'images/[path][name].[ext]'
})
```

Il faudra installer ensuite file-loader : 

```bash
yarn add file-loader@^6.0.0 --dev
```

On peux ensuite charger notre image dans nos vues de cette manière : 

```html
<img src="{{ asset('build/images/mon_image.jpg') }}" alt="">
```

## Ouf !

Une fois que c'est fait, on peux bosser sur le projet en tappant  :

```bash
yarn watch
```
?> A chaque modif', le projet se regénère :white_check_mark:

<hr>

## Recap : Lancer un projet Symfo avec Webpack

**Terminal 1**

```bach
git clone 
-- si on recupère un nouveau projet --


git pull
-- si on recupère des modifs depuis un projet existant --


composer install
-- si jamais ya eu des ajouts de dépendances dans composer.json par ex --


yarn watch
```

?> Check les **modifs** sur le projet et **recompile** à chaque fois


**Terminal 2**

```bach
symfony server:start
```

?> Lance le **serveur de Symfony** avec notre site

On est prêts à bosser :ok_hand:

<br>

**Rappel** : Si on veux lancer un serveur symfony sur un **autre port** :

```bash
symfony server:start --port=8078
```

?> Symfo tourne alors sur le port **8078** et pas 8080 *(le port par default)*

