# PDO - PHP Data Objects

Le PDO défini une interface pour **accéder à une base de données depuis PHP**. Il permets d'exécuter des requêtes ou récupérer les données *(cf cours de SQL pour les requêtes)*.

## Se connecter à une BDD

On commence par faire un `try catch` pour se connecter à la base de données :

```php
try {
    $db = new PDO('mysql:host=localhost;dbname=adrec_pdo;charset=utf8', 'root', 'root');
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    echo $e->getMessage();
    die('Erreur base de données');
}
```

?> Dans le `try`, on se connecte à la BDD en rentrant sont **host** *(localhost ici)* et son **dbname** *(adrec_pdo ici)*, on renseigne ensuite son *nom d'utilisateur* et *son mot de passe*<br>
Le `catch` **attrape** l'erreur s'il y en a *(un mauvais mot de passe par exemple)*.<br>
Il vaut mieux faire un `die` ensuite avec un message d'erreur.

## Faire des requêtes sur une BDD

### Requête de base

La réponse d'une requête doit être stockée dans une `var` *($response par exemple)* 

`query` = **requête**, on fait ici la requête `SQL`sur la BDD,<br>
`response` = **réponse** de la BDD,

A partir de la reponse, le `fetchAll` récupère **toutes les lignes sous forme de tableau**

Si on veux la **reponse en Objet** on le fait avec l'argument `PDO::FETCH_OBJ`

Quand on fait un `fetchObject` on ne peux pas l'exploiter tel quel, du coup on fait un `while` qui boucle pour afficher chaque ligne<br>

```php
$response = $db->query('SELECT * FROM `user`');

// Version avec un fetchAll
$users = response->fetchAll(PDO::FETCH_OBJ);

foreach ($users as $user) {
    echo $user->username;
    echo '<br>';
    echo $user->mail;
    echo '<hr>';
}

// Version avec un fetchObject
while ($user = $response->fetchObject()) {
   echo $user->username;
   echo '<br>';
   echo $user->mail;
   echo '<hr>';
}
```
?> On recupère ici le **username** et le **mail** de la base de donnée

> Derrère un `fetchAll`, c'est un `foreach` <br>
> Derrière un `fetchObject`, c'est un `while`


### Différence entre *Query* et *Prepare*

Pour l'instant on a vu les requêtes en passant par `query` mais il existe aussi le `prepare`.

#### Quand utiliser l'un ou l'autre ?

##### Query

On utilise `Query` quand c'est une **requête pour nous**, on connait la valeur demandée

```php
$response = $db->query("SELECT * FROM user WHERE active = 1 ORDER BY username");
```

?> Ici on sait qu'on recherche un `active` à `1`, on le précise directement

<br>

##### Prepare

*De base, on évite de laisser les utilisateurs envoyer des requêttes vers la BDD (logique hein)*

`Prepare` est utilisé quand il s'agit d'une **requête d'un utilisateur**, on **prépare** la commande et la valeur demandée est envoyée via le `execute`:

```php
$req = $db->prepare("SELECT * FROM user WHERE active = ? ORDER BY username");
$req->execute([1]);
```

?> Ici on ne sait pas quelle valeur de `active` sera recherchée<br>
On prépare la recherche et via le `execute`, la valeur `1` sera précisé par l'utilisateur et remplacera le point d'interrogation *(via un `POST` dans un formulaire sûrement).*

<br> 

**Autre exemple**

```php
// Simulation des données du formulaire -> $_POST['passwd']
$formPassword = 'admin';

$req = $db->prepare('SELECT username, password FROM `user` WHERE username = ? LIMIT 1');
// Le point d'interrogation est remplacé par une entrée de la bdd
$req->execute(['admin']);
// Ici, il s'agit de 'admin'

while ($user = $req->fetchObject())
{
    if(password_verify($formPassword, $user->password)){
        echo 'Faire la connexion';
        //$_SESSION['logged_in'] = true;
    } else {
        echo 'Connexion impossible';
    }
}
```

?> Cette commande permets de récupérer un utilisateur dans la base avec une requête préparée, puis de l'afficher.


## Nommer les marqueurs dans un requête

Si la requête contient beaucoup de parties variables, il peut être plus pratique de nommer les marqueurs plutôt que d'utiliser des points d'interrogation par exemple:

```php
$req->execute(array(
    'entree1' => $_GET['entree1'],
    'entree2' => $_GET['entree2']
    ));
```

Ca permet de rendre plus claires les données de la BDD.

On peux aussi faire un tableau où on associe les `champs` à des `variables` :<br> 
*Les variables doivent avoir été définies précédemment* 

```php
$req->execute(array(
	'nom' => $nom,
	'possesseur' => $possesseur,
	'console' => $console,
	'prix' => $prix,
	'nbre_joueurs_max' => $nbre_joueurs_max,
	'commentaires' => $commentaires
	));
```
