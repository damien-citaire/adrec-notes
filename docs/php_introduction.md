# Infos de base & diverses


## Installation

php.net/downloads.php _pour Windows_

## Lancer php

### Lancer php dans un terminal

```
php -a
```

### Lancer un serveur local PHP

```bash
cd index.php;
php -S localhost:8099
```

?> Lance le serveur php à l'adresse localhost:8099


## Les variables

```php
// Assignation d'une variable de type "chaîne de caractères"
$firstName = "Théau";
// Assignation d'une variable de type "entier"
$age = 33;
// Assignation variable de type "booléen"
$isAlive = true;
// Création d'une variable de type "tableau"
$array = [];
// Création d'une variable nulle
$null = null;
```


### Affichage d'une variable

```php
<?php

/**
 * Better than var_dump
 * @param $var
 */
function dump($var): void
{
    echo '<pre>' . var_export($var, true). '</pre>';
}


$string = 'Hellooo';
$array = array(1, 2, array("a", "b", "c"));

echo'<h2>Affichage string</h2>';

echo'<h3>echo</h3>';
echo $string;

echo'<h3>print</h3>';
print $string;

echo'<h3>print_r()</h3>';
print_r($string);

echo'<h3>var_dump()</h3>';
var_dump($string);

echo'<h3>dump() - <i>Custom function()</i></h3>';
dump($string);

echo '<hr>';

echo'<h2>Affichage array</h2>';

echo'<h3>echo</h3>';
echo $array;

echo'<h3>print</h3>';
print $array;

echo'<h3>print_r()</h3>';
print_r($array);

echo'<h3>var_dump()</h3>';
var_dump($array);

echo'<h3>dump() - <i>Custom function()</i></h3>';
dump($array);
```

?> Affichage en retour :<br><br>
**Variable string**
![](img/php_display-string.png)
<br>
**Variable array**
![](img/php_display-array.png)


### Various tips :

- `echo` et `print` sont pratiques pour des chaine de caractères, ils font la même chose, _mais on utilise plutôt `echo`._

- `print_r` et `var_dump` affichent le contenu d'un variable. _(`print_r` le fait sous forme de **string**.)_

- `var_export` affiche le tableau en une ligne <br> `var_dump` est le plus précis dans l'export d'un tableau en "joli" *(sur plusieurs lignes)*

- Le `dump()` est à faire avant un `return`

- Pour compter les caractères d'une variable on utilise : <br> `echo strlen($mavariable);`

- Pour faire un retour à la ligne dans un `echo` on fait un `\n`.

- On peux caler des **variables** dans une **chaine de caractères** sans concaténer avec les `.` si on est dans des **doubles quotes**. Mais en général on passe quand même par les concaténations en `.` <br> *Et on oublie si la variable est un tableau par exemple.*

- Quand on affiche du **texte**, on passe plutôt par des **doubles quotes**, et quand on affiche du **code** on passe plutôt par des **simples quotes** _(c'est moins coûteux en mémoire)_


### La duplication de variables

On peux dupliquer une variable pour editer seulement **une des 2 itérations**, on ne modifie pas le même objet tout en ayant la même valeur à la base.

```php
$varToBeModified = clone $var;
```

?> Duplique `$var` sous le nom `$varToBeModified`*


## Les opérateurs

```php
// Addition : +
$result = 2 + 5;
// Soustraction : -
$result = 5 - 5;
// Multiplication : *
$result = 5 * 5;
// Division : /
$result = 8 / 2;
// Modulo : % (donne le reste de la divison)
$result = 8 % 2;
```

<br>
Quand on fait un calcul, le `+=` rajoute à la variable ce qu'on lui passe,<br>

```php
// Version 1 : 
$number = 2;
$number = $number + 2;

// Version 2 avec += : 
$number = 2;
$number += 2;
```

?> Les deux versions font la même chose et renveront *(2 + 2) =*  **4**

*Pareil pour `*=` en multiplication, `-=` en soustraction et `/=` en division*

### Incrémentation

```php
echo '<h3>Post-incrémentation</h3>';
$a = 5;
echo "Devrait valoir  5: " . $a++ . "<br />\n";
echo "Devrait valoir  6: " . $a . "<br />\n";
echo '<h3>Pre-incrémentation</h3>';
$a = 5;
echo "Devrait valoir  6: " . ++$a . "<br />\n";
echo "Devrait valoir  6: " . $a . "<br />\n";
echo '<h3>Post-décrémentation</h3>';
$a = 5;
echo "Devrait valoir  5: " . $a-- . "<br />\n";
echo "Devrait valoir  4: " . $a . "<br />\n";
echo '<h3>Pre-décrémentation</h3>';
$a = 5;
echo "Devrait valoir  4: " . --$a . "<br />\n";
echo "Devrait valoir  4: " . $a . "<br />\n";
```


## Les if

```php
if ($result >= 50) {
echo "Your result is at least the average.";
} else if ($result >= 90) {
echo "Your result is very good.";
} else {
echo "Your result is not bad.";
}
```

Quand on fait des `if`, on peux ne pas être sûrs à 100% de couvrir tout les `elseif` possibles. Du coup on mets souvent un dernier `return` à `null` ou avec un message du type `"J'ai pas pensé à tout"` :innocent: dans un `else` histoire de savoir qu'on n'est dans aucun des cas qu'on cite avant`.

*Cette pratique est surtout valable si on est avec un argument de type string, en booleen par exemple on s'en fout.*

### Les ternaires

Autre syntaxe, moins longue pour les if : 

```php
echo ($result > 50) ? "Your result is above 50!" : "Your result is under 50...";
```

## Les boucles

### Les boucles for et foreach

Les boucles `for` ou `foreach` se traduisent par *"pour"*.

`for` tourne tant que la condition n'est pas remplie.
`foreach` est surtout utile quand on parcourt un tableau.

<br>

#### La boucle `for`

```php
for($i = 0; $i <= 10; $i++) {
    echo $i . ' ';
}
```

?> • Avec le **1er argument**, on defini `i` à `0`. <br>
*C'est notre point de départ pour la boucle.* <br>
<br>
• Avec le **2eme argument**, on dit qu'on veut que `i` soit `inférieur ou égal à 10`. <br>
*C'est notre **condition** pour sortir de la boucle.* <br>
<br>
• Avec le **3eme argument**, on dit qu'on ajoute `1` à `i` <br>
*C'est ce qui va se passer à chaque itération/répétition de la boucle.* <br>
<br>
<br>
Cette boucle `for` va donc partir avec un `i` à `0` et <br>
lui rajouter `1` **autant de fois que nécéssaire** <br>
jusqu'à ce que `i` ne soit plus `inférieur ou égal à 10`<br>
<br>
*Le `echo` présent au sein du `for` nous montrera chaque étape.*<br><br>
On aura donc en retour ceci : 
![](img/php_boucle-for.png)

<br>

#### La boucle `foreach`

```php
$values = array(1, 2, 3, 4);

foreach ($values as $value) {
    $value += 10;
    echo $value . ' ';
}
```

> Comme précisé plus haut, `foreach` sera utilisé pour des tableaux et des objets.

?> *Ici on parcourt le tableau `$values`* <br>
<br>
• Chaque entrée de `$values` est traitée comme une `$value` <br>
<br>
• Pour chacune des `$value`, on lui ajoute `10` et on la retourne<br>
<br>
<br>
*Le `echo` présent au sein du `foreach` nous montrera chaque étape.*<br><br>
On aura donc en retour ceci : 
![](img/php_boucle-foreach.png)

### Les boucles while et dowhile

Les boucles `while` ou `dowhile` se traduisent par *"tant que"*. 

Ils continuent tant que la condition n'est pas remplie<br>
*Gaffe aux boucles infinies*

> Pour arreter complètement l'execution d'une boucle, on fait un `break`.

#### La boucle `while`

```php
$target = 5;
$random = rand(1, 10);

echo "<strong> Objectif : trouver " . $target . "</strong><br><br>";

while ( $random != $target ) {
    echo $random . " -> Perdu <br>";
    $random = rand(1,10);
}

echo "<hr><strong>" . $random . " -> Gagné !</strong>";
```

?> On a une variable `$target` fixée à `5` et une variable `$random` qui est un `random entre 1 et 10`<br>
<br>
*L'objectif est que `$random` soit **égal** à `$target`*<br>
<br>
A chaque tour de boucle, un nouveau random sera fait et cette dernière ne se stopera que lorsque l'objectif sera rempli. <br>
<br>
*Le `echo` présent au sein du `while` nous montrera chaque étape.*<br><br>
On aura donc en retour ceci : 
![](img/php_boucle-while.png)<br>
*Le nombre de tentatives sera différent à chaque rechargement de la page.*

> Dans l'exemple ci-dessus on ne sait pas combien de fois la boucle sera parcourue, d'où l'utilisation du `while`.


### Les switch

Les switch vous permettent d'effectuer une opération en fonction de la valeur prise par une variable.

```php
switch ($i) {
    case "apple":
        echo "i est une pomme";
        break;
    case "bar":
        echo "i est une barre";
        break;
    case "cake":
        echo "i est un gateau";
        break;
}
```

?> Si $i est un `"cake"`, alors le retour sera `"i est un gateau"`.


## Les fonctions

- Dans une fonction, on peux passer les types suivants : **string**, **int**, **float**, **variable**...


- **La différence entre `echo` et `return`:** <br>
Dans une fonction il vaut  mieux utiliser un `return`, le `echo` c'est plus pour un rendu final. Le `echo` ne sais pas donner les resultat d'une fonction.

- Si on met `void`, ça veux dire qu'on ne retourne rien.<br> *Par exemple si notre fonction appelle une autre fonction qui, elle, retourne quelquechose, alors là le void est justifié car la fonction de base ne fait qu'appeler l'autre fonction.*

- Pour créer un paramètre oprtionnel on lui donne une valeur par defaut : `function toto($param1, $param2 = "coucou")` par exemple.

- Faire un dump d'une fonction de php permet de voir tout ce qu'on peux faire avec *(on peux aussi aller sur la doc hein)*



## Les Tableaux

Dans un tableau, on compte l'index à partir de `0`. <br> Si, par exemple, on veux 100 valeurs on peux faire un `$i = 0; $i < 100; $i++` dans un `for`.<br>*Le `>` fait qu'on s'arrete à 99 du coup avec le 0 ça fait 100.*

Dans un tableau, `$key` nous donne le **numéro de case** de l'entrée.

Pour ajouter une valeur dans un tableau venant d'un autre tableau, la syntaxe est la suivante : <br>
`$monTableau[] = $autreTableau[$uneEntreedeLautreTableau]`.

On peux retirer une valeur d'un tableau avec `unset`.
