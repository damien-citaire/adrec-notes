# CRUD

## Ajout de templates aux formulares

Avant d'attaquer le CRUD on va s'assurer que les formulaires qui vont être générés le seront avec un template de chez Bootstrap on va donc aller dans `config/packages/twig.yaml` et s'assurer que le style pris en compte vienne de ce dernier en générant les bonnes classes pour les formulaires. On rajoute donc `form_themes: ['bootstrap_4_layout.html.twig']` *(gaffe à l'indentation, c'est du yaml)*.

Le fichier ressemble à ca maintenant :

```yaml
twig:
    default_path: '%kernel.project_dir%/templates'
    form_themes:
            - 'bootstrap_4_layout.html.twig'
```

## Génération du CRUD

```bash
symfony console make:crud nom_classe
```

?> Cette commande **génère des fichiers** permettant de gérer le CRUD.

<br>

Admettons qu'on tape cette commande pour l'entity **Classes** qui aura été générée en amont *(plus d'infos sur la génération d'entity [ici](/symfony_entities))*.

On retrouve dans les templates tous les ecrans avec des tableau pour **ajouter**/**consulter**/**editer**/**supprimer** des infos dans les champs de la table **Classe** *(tout est fait bordel ! :open_mouth:)*

<br>

Le style est ultra minimaliste pour l'instant mais on peux embarquer **bootstrap** via notre fichier de config `twig.yaml` (avec `form_themes`)




Dans `config/packages/twig.yaml` il faut ajouter `form_themes` avec le nom du template de formulaire à integrer *(yen a pleins dans vendor graçe à `twig-bridge`)*, on a ça : 

```
twig:
    default_path: etc
    form_themes: ['bootstrap_4_layout.html.twig]
```

Dans la vue, initialiser un **tableau** de cette manière `{% form_start(nom_du_form) %}`
avec le **nom du template** *(un de ceux ajoutés dans `config/packages/twig.yaml`)*

Pour l'intégrer dans le form il faut ajouter le  theme avec ça : `{{ form_themes form 'bootstrap_4_layout.;html.twig' }}`



## Ajouter des champs qui ne font pas partie de l'entity

Dans le builder de nos formulaires, on peux aussi ajouter des champs même s'ils ne sont **pas présents** dans notre entity *(un slug par exemple)*

On l'ajoute comme ça dans notre `src/Form/ClassType.php` : 

```php
-> add('slug', TextType::class, [
    'label' => 'Slug',
    'mapped' => false,
])
```

?> le `'mapped' => false` est important dans ce cas car même s'il ne fait pas partie de notre objet on le veux tout de même. Si on ne le mets pas on a une erreur car il n'est pas **mapped**. 

> *Rappel* -> **Map** = tableau d'association clé - valeur

**Exemple, le map de base du form doit ressembler à ça en gros :** 

```php
$form['fields'] = [
    ['key_field_name'] => 'entity_field_name'],
    ['key_field_label'] => 'entity_field_label'],
    ['key_field_level'] => 'entity_field_level'],
]
```

?> Du coup on se doute que notre `slug` qu'on veux ajouter n'est pas dans le tableau, d'où le `'mapped' => false`.