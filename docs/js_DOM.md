
# Manipulation du DOM

## Récuperation

### querySelector

`querySelector` permets de récupérer via des balises (selecteurs css).
*On a aussi getElementById, mais c'est dinosaure, on laisse tomber.*

```js
document.querySelector('body > div.container');
```

> On peux aussi prendre un autre point de départ que document (qui cherche sur toute la page)

?> Ici on chope la div avec la classe container dans body

### .style

`style` permets de changer le ... *wait for it*   ...  style :smirk: 

```js
const $container = document.querySelector('body > div.container');
$container.style.background = 'red';
```
?> On cible container dans body via une const et on change le style pour mettre un background en rouge<br>
On ecrit les styles en **camelCase**.

**On peux aussi faire :**

```js
const val = $container.style.fontSize;
```
?> Permets de récupérer la valeur `font-size` de `$container`, en une seule étape

### setTimeout

`setTimeout` prends 2 arguments : une fonction anonyme **handler** *(ce qu'on va faire)* et **timeout**, où on donne le timing

#### Rappel fonction anonyme
Une fonction anonyme à cette forme : `() => {}`

```js
const $body = document.querySelector('body');
setTimeout( () => {
    $body.style.background = 'blue'
}, 5000);
```

?> On selectionne body, et, au bout de 5 secondes, on change son background en red

<br>

**Autre exemple :**

```js
const $body = document.querySelector('body');

// On créé un array de couleurs
const colors = [
    'red',
    'blue',
    'green',
    'orange',
]

// On créé une fonction qui pioche une couleur de manière aléatoire
// -- Pour info Math.random() tape entre 0 et 1, pas pratique pour 
// -- choper l'index, il faudra faire un Math.ceil(rand * 8) pour
// -- s'en sortir
function randomColor() {
    const rand = Math.random();
    const index = Math.ceil(rand * 8);

    return colors[index]
}

function displayColor(color, delay) {
    setTimeout( () => {
        $body.style.background = color;
        displayColor(randomColor(), delay);
    }, delay);
}

displayColor(colors, 1000);
```

?> On affiche un bckgrd color différent toutes les 10 secondes, la couleur est piochée en random dans un tableau colors


### querySelectorAll

Recupère l'ensemble des selecteurs qui correspondent sous forme d'un *tableau* (NodeList).

```js
elements = document.querySelectorAll('div');
console.log(elements);
```
?> chope toutes les divs et les listes sous forme de tableau.

### .classList

```js
const $emailInput = document.querySelector('.form-group #email');
$emailInput.classList.add('text-success');
```

?> Ajoute la class `text-success` à la div avec l'ID email enfant de la div avec la class form-group.

On a `add` mais peut aussi faire `remove` ou `toggle` par exemple

### Quelques exemples et suppléments

```js
const $emailInput = document.querySelector('input#email');

$emailInput.classList.add('text-danger');
// Ajoute la classe CSS à la balise

$emailInput.classList.remove('text-danger');
// Enlève la classe CSS de la balise

$emailInput.classList.toggle('text-danger');
// Ajoute la classe CSS à la balise si elle n'est pas présente et inversement.

$emailInput.classList.contains('text-danger');
// return Boolean, true si la classe est présente dans la balise.

$emailInput.value;
// Récupère la valeur d'un champ de formulaire.

$emailInput.value = 42;
// Change la valeur d'un champ de formulaire.

$emailInput.id;
// Récupère l'id d'une balise.

$emailInput.id = 'nrsaurnisrnauirnsaui';
// Change l'id d'une balise.
```

```js
const $container = document.querySelector('div.container');

$container.innerText;
// Récupère les textes présents dans la balise.

$container.innerHTML;
// Récupère tout le code HTML présent dans la balise sous forme de string.

$container.innerText = 'Bonjour';
// Remplace tout le contenu de la balise par le texte donné.

$container.innerText = '<h1>Bonjour</h1>';
// Tous les caractères sont traités comme du texte simple.

$container.innerHTML = '<h1>Bonjour</h1>';
// Remplace tout le contenu de la balise,
// en convertissant le texte en nouvelles balises si possible.

$container.parentElement;
// Récupère la balise parente.
$container.parentNode;
// idem.



const $h1 = document.querySelector('h1');

$h1.parentElement;
// Récupération de la balise parent à notre sélecteur
$h1.parentElement.parentElement;
// On peux les cumuler

$container.children;
// Récupère la liste des enfants directs de la balise.
$container.childNodes;
// Idem (à privilégier)

$container.querySelector('h1');
$h1.closest('div');
// Similaire à querySelector (mais en inversé).
// Récupère parmis les ancètres de la balise, la première qui correspond au selecteur.
```

> `innerText` affiche le contenu sous forme de texte.

> `innerHTML` annalyse le texte et convertit ce qu'il peux en html si possible<br>
Il remplace le contenu, c'est pas archi-top *(sauf si on veux remplacer hein)*.

### Exemple rapide de deux commandes en une seule

```js
const $phoneInput = document.querySelector('#phone').parentElement;
```

?> Permets de créer une const `$phoneInput` et de recupérer son element parent *(ainsi que son contenu)*.<br>
On récupère tout ce qui le wrappe directement au final.


## Creation, Edition, Suppression...

### Pour créer un élément

Si on veut **ajouter** du contenu, on fais plutôt de cette façon :


```js
const $container = document.querySelector('div.container');
// Je recupère un container existant

const $newDiv = document.createElement('div');
// Je créé une div

$newDiv.innerText = 'Bonjour à tous';
// Je lui joute du texte

$newDiv.classList.add('text-success');
// Je lui ajoute une classe css

$container.appendChild($newDiv);
// Insertion de la nouvelle balise en dernier enfant de container
// J'ajoute la div et son contenu à la fin de container

$container.prepend($newDiv);
// Insertion de la nouvelle balise en premier enfant de container
```

> On peux ajouter notre div **au debut** de notre conteneur en mettant `prepend` à la place de `appendChild` si on veux.


Pour le texte on peux aussi utiliser `createTextNode` :

```js
const $text = createTextNode('Bonjour à tous');
```
?> C'est un poil + long car il va falloir le append directement à notre p ensuite alors qu'avec `.innerText` le texte est balancé dans le p en une seule étape.

<br>

### Placer ce que l'on fait de manière plus précise

On reprends ce qu'on a fait avant *(la const container, la div)*<br>
On utilise `insertBefore()` qui prends 2 arguments :
- le 1er indique l'element que l'on veux inserer
- le 2eme indique l'element avant lequel on veux insérer notre élément en 1er argument, son parent direct.

```js
const $form = document.querySelector('form');
$form.insertBefore($newDiv, $phoneDiv);
// Insertion de la nouvelle balise dans la balise form, avant la balise phone.
```
?> **$form** sera le parent direct<br>
**$newDiv** sera celui qu'on insère<br>
**$phoneDiv** sera la balise se trouvant avant **$newDiv**

### Pour enlever un élément

```js
$phoneDiv.remove();
// Enlève du dom la balise (la balise reste disponible en JS)
```

?> Enlève la const $phoneDiv du DOM *(mais elle n'est pas complètement supprimé, on peux toujours la rappeler)*