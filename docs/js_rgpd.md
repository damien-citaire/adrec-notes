# RGPD

## Exemple d'une modal de RGPD

> On est en procedural là

Dans notre html, on a cette modal : 

```html
<div id="gdpr-modal" class="modal">
    <div class="modal-content">
        <div class="modal-title">
            Suivi RGPD
        </div>

        <div class="modal-body">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit.
        </div>

        <div class="modal-choices">
            <button type="button" class="btn-accept" data-tracking-click>
                Accepter
            </button>

            <button type="button" class="btn-refuse">
                Refuser
            </button>
        </div>
    </div>
</div>
```

On appelle ce JS : 

```js
// On créé les variable pour choper les éléments html qui nou intéressent
const $refuseBtn = document.querySelector('#gdpr-modal .btn-refuse');
const $acceptBtn = document.querySelector('#gdpr-modal .btn-accept');
const $gdprModal = document.querySelector('#gdpr-modal');

function closeModal() {
    // Fermer la modal
    // on retire la classe open
    $gdprModal.classList.remove('open');
}

function openModal() {
    // Ouvrir la modal
    // on ajoute la classe open
    $gdprModal.classList.add('open');
    // $gdprModal.style.removeProperty('display');
}

// -> La classe open change le display qui est à none par defaut sur la modal

function initTrackers() {
    // on récupère nos traqueurs :
    // soit : Google Analytics,
    // soit : Matomo
}

$refuseBtn.addEventListener('click', () => {
    // on dit au localstorage que c'est non pour la rgpd
    localStorage.setItem('gdpr', 'ko');
    // on ferme la modal
    closeModal();
});

$acceptBtn.addEventListener('click', () => {
    // on dit au localstorage que c'est oui pour la rgpd
    localStorage.setItem('gdpr', 'ok');
    // du coup on bascule dans le function qui gère cette partie
    initTrackers();
    // on ferme la modal
    closeModal();
});


// On chope le localStorage lié à la rgpd
const accept = localStorage.getItem('gdpr');
// on check s'il est null ou pas
if (accept === null) {
    // s'il est null on ouvre la modal pour poser la question
    openModal();
} else if (accept === 'ok') {
    // s'il est sur ok on bascule dans le function qui gère la rgpd
    // (pas besoin de check s'il est à non, d't'façon dans ce cas on ne fait rien)
    initTrackers();
}

```