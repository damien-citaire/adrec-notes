# Storage

## Presentation

Ces notions sont par exemple utiles pour :
- la **rgpd**, *(avec le bandeau des cookie)*,
- les **themes** dark ou light,
- les **paniers** (même si de moins en moins *(c'est plutôt en BDD maintenant)*),
- une **préférence de langue** *(fr, en ...)*,
- **etc**

On a une variable `localStorage` et une variable `sessionStorage`.

Le **stockage de session** est **effacé quand on ferme notre navigateur**.<br>
Le **stockage local** est **enregistré tant qu'on n'a pas vidé le cache**.


On peux les visualiser dans la *console chrome*, onglet *Application*, section *Storage*.

Dans les storages, on ne peux stocker **que des strings**, *si on passe un number ce sera entre quote, sous la forme d'un string.*

**Exemples :**

```js
localStorage; 
// Garde en mémoire tant que le cache du navigateur n'est pas vidé.
sessionStorage;
// Garde en mémoire tant que le navigateur n'est pas fermé.
// -> Même usage pour les 2.


const value = localStorage.getItem('foo');
// si n'existe pas, null retourné


localStorage.setItem('foo', 'vjlaunrisnrautirntr');
// Ajoute une valeur

localStorage.setItem('foo', '42');
// Seul des strings peuvent être stockés

localStorage.setItem('foo', JSON.stringify({
    foo: 'bar',
    bar: 42,
    saveAt : '2020-12-14',
}));
const obj = JSON.parse(localStorage.getItem('foo'));
//on peux passer par du Json aussi


const removedValue = localStorage.removeItem('foo');
// enlève la clé et retourne la valeur


localStorage.removeItem('foo');
// enlève la clé et retourne la valeur


localStorage.clear();
// vide le localstorage pour votre site (pas les autres)
```

## Exemple RGPD bandeau

```js
const gaCheck = localStorage.getItem('analytics_gdpr_accept');
// yes | no

if (gaCheck === null) {
    // Ouvrir la modale de RGPD
}

if (gaCheck === 'yes') {
    // Code de Google Analytics
}
```

?> On check dans a console si on a bien `yes` à `analytics_gdpr_accept`.
![](img/js_storage_gdpr-example.png)
