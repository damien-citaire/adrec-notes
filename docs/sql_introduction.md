# SQLite

## Les concept fondamentaux

Une base de données sert à stocker des informations. On peux les organiser facilement, y effectuer des recherches et différentes actions telles que : **CRUD** &rarr; Create / Read / Update / Delete

**Requêter** : permet de lire le contenu de la **B**ase **D**e **D**onnée

## Les différents types de BDD

- BDD Navigationnelle *(obsolète)*
- BDD Relationnelle *(souvent orientée objet)*
    - apparition des tables
    - possibilité de communication entre les tables
- BDD orientée objets
    - principe de la prog orientée objets
    - un objet peux correspondre à une table
    - basé sur du relationnel
- NoSQL *(not only SQL)*
    - ne gère pas les relation entre les tables
    - système distribué
    - rapidité de l'accès à l'info
    - permet d'editer à plusieurs simultanement la BDD, contrairement aux autres BDD
- BDD Clé-valeur
    - on retrouve le NoSQL
    - système d'identifiant clé = valeur

## Les systèmes de gestion de BDD

**SGBD** (système de gestion de base de données, *DBMS en anglais, Database Management System* <br>
Permet de structurer/partager la BDD.

**Les différents SGBD :** 
- SQlite (plus léger, plutôt utilisé sur mobile de par les restrictions lié à ce type d'appareil)
- MySQL (le principal)
- Oracle (payant, permet notament de stocker des donnnées spatiales, de localisation)
- Microsoft SQL Server (propriétaire aussi)
- MongoDB
 