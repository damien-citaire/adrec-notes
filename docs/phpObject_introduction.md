# PHP Objet

## Présentation

Une classe peut être vue comme un regroupement de variables.

Quand on définit une classe, on définit un nouveau type *(comme int, string, etc)*.<br>
Par exemple une classe `Contact` defini un nouveau type **Contact**.

Une classe est une **idée/concept** qu'on modèlise en informatique.<br>
On peux comparer **une classe et ses attributs** avec **un champs et des tables**  en `sql` par exemple.<br>
*On peux voir aussi une classe comme un tableau.*

Dans une classe, une **variable** s'appelle **attribut**.


## Les conventions de nommage

- On ecrit la classe avec une **majuscule**
- On ecrit une variable en mettant toujours la **première lettre en minuscule** puis le reste en **camelcase**
- L'ordre dans la classe est important : **attribut**, **constructeur** si necessaire et enfin les **méthodes**
- Pour les `Get` et les `Set` : le `Get` en **1er** et `Set` en **2eme** <br> *(bon après ça c'est généré direct par PhpStorm hein)*


## La visibilité

On définit qui a le droit de voir la classe, sa methode, ses attributs. On le fait via `public`, `private` et `protected`.

- `public` Accessible depuis **partout**, aussi bien la classe courante que les autres.
- `protected` Accessible uniquement par la classe **courante et ses filles** *(**héritage**)*.
- `private` Accessible uniquement par la classe **courante**. <br>


## Les **attributs** *(visibilité private ou protected)*

Les **attributs** ne sont modifiables qu'**au sein de notre classe**, ils seront donc soit `private`, soit `protected` *(dans le cas où la classe concernée aura des enfants, extends*.


### Poussons un peu le *protected*

`protected` : l'attribut ou la méthode ne sont accessible que via ses classes filles (*héritage*).<br>
Les classes filles `extends` la classe mère, exemple :

```php
class Mp3 extends Chanson
{
  // interieur de la classe
}
```
?> Ici la classe **Mp3** est la classe fille de **Chanson**, elle récupère tout ce qui compose Chanson*<br>
*Du coup via Mp3 on peux recupérer ce qui est privé dans Chanson*

## Les **methodes** *(visibilité public)*

Les **methodes** de la classe seront en `public`, c'est elles qui permettent d'accéder à la classe.


### Les **getter** et les **setter**

On peux tout de même appeler les attributs à l'extérieur de la classe pour les modifier graçe aux methodes **setters** et aux methodes **getter**, ça permets de les preserver *(les attributs)*.

Pour chaque attribut on a donc un **getter** `getAttribut` et un **setter** `setAttribut`.<br>
Ces deux-là sont forcément **public**.

> Pas besoin de setter pour un id

!> **Getter** on **renvoie** une info donc `return`, ne prend pas de paramètres<br>
**Setter**, ne return rien, on **modifie** quelque chose donc on prend un **paramètre** pour l'assigner


