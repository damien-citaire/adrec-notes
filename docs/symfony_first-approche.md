# 1ère Approche

## Démarage

Symfony est dispo [ici](https://symfony.com/download)

### Créer un projet

Une fois que Symfony est bien installé, on peux **créer un projet** en tapant la commande suivante : 

```bash
composer create-project symfony/website-skeleton nom_projet
```

<br>

Si jamais on récupère un projet Symfony **existant**, il faut d'abord faire un :

```bash
composer install
```

?> Cette commande permet à Symfony de récupérer les **dépendances** données par le composer.json *(coucou le dossier `vendor`)*

!> Au cas où, ajouter les `.DS_Store` et autre `.idea` au `gitignore` avant de commiter la 1ère fois.

#### Apparté BDD :

> :exclamation: La configuration vers la BDD est **vitale** pour bosser avec Symfo :exclamation: <br>

Elle se trouve dans notre fichier `.env` (ou `.env.local`si on prefere) : 

```
DATABASE_URL="mysql://root:pass@127.0.0.1:8889/ma-bdd?serverVersion=5.7"
```

?> Ici, on dit qu'on est en **mysql**, que l'utilisateur de la BDD est **root**, que le mots de passe est **pass** et que notre BDD est accessible à l'adresse **127.0.0.1:8889**, qu'elle se nomme **ma-bdd** et que le serveur est en version **5.7**.
Toutes ces infos sont **importantes** pour que tout fonctionne correctement.

On peux faire tourner symfo avec ces info sans que la BDD soit créee pour autant *(tant que php my amin tourne c'est ce qui compte)*. Bien sûr, plus vite le BDD existera mieux ce sera. On peux d'ailleur générer la BDD en tapant la commande suivante : 

```bash
symfony console doctrine:database:create
```

?> En tappant cette commande, une BDD avec le nom **ma-bdd** sera créée :sunglasses:

Pour info, si on veux **delete** notre BDD on peux le faire avec : 

```bash
php bin/console doctrine:database:drop --force
```


### Lancer un serveur Symfony

```bash
symfony server:start
```

?> Nous permets de lancer le serveur et d'accéder à la page d'accueil par defaut de Symfony.

<br>

> Par defaut, le server tournera sur le **port 8000**, mais on peux spécifier un **autre port** en tapant cette commande : 

```bash
symfony server:start --port=8005
```

Tu en veux plus ? Suis ce [lien](https://symfony.com/doc/current/setup/symfony_server.html)


## Commencer sérieusement

Maintenant qu'on a lancé Symfo sans rien, on va le remplir un peu :

- Go `src/Controller` et on y créé notre **1er controller** (`HomeController.php`), ce sera une **class**.<br>
Cette class **extend** `AbstractController` *(déjà présent dans Symfo)*

!> Un Controller doit toujours extends de **AbstractController**


<br>

- On créer maintenant une `public function homePage()` qui servira de route au sein de notre class `HomeController` nouvellement créée.

    On balance ensuite les commentaires au dessus de cette fonction et on mets `@Route()`.<br>
    On fait ensuite `CTRL + Space` sur `@Route()`, ca va automatiquement **ajouter l'import necessaire pour le routing**.

<br>

- Toujours dans les commentaires de la fonction, on rajoute `path="/", name="home_page"` pour définir notre route avec un **chemin** et un **nom**.

    En l'etat actuel, si on check sur notre navigateur, on a une erreur. On peux avoir des détails en allant sur l'onglet `Logs`<br>
    On a également une **barre d'etat** en bas qui donne pas mal d'infos. La zone de **debug** pour le routage se situe sur le nom de la page *(sur la gauche de cette barre)*.


<hr>

>### Apparté routage
>Symfo fonctionne par un système de **Routage**, ce système permets aux controller d'**interagir** entre eux, de chopper des vues etc, il permets de **naviguer dans notre application**. Par défaut il veut toujours une route `/` *(corespondant au home)* <br><br>
> **Lister toutes les routes** d'un projet symfo :  <br>
```bash
php bin/console debug:router
```

<hr>

- Pour aller plus loin et resoudre ce soucis, on continue en ajoutant `: Response` à la suite de `public function homePage()`.<br>
On refait `CTRL + Space` et on selectionne `HttpFoundation` pour avoir l'import de `Response`.

!> Un Controller doit renvoyer un objet de type **HttpFoundation/Response**

<br>

- On ajoute ensuite dans notre function la ligne suivante : `return $this->render('base.html.twig');`<br>
Cela nous permets d'**appeler le fichier** `base.html.twig` présent dans `templates` et de l'**afficher**.

<br>

**Notre fichier `HomeController.php` ressemble à ça à l'heure actuelle :** 

```php
<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{

    /**
     * @Route(path="/", name="home_page")
     */
    public function homePage(): Response
    {
        return $this->render('base.html.twig');
    }

}
```
?> Avec ça, on est en mesure d'**appeler** et d'**afficher** le fichier `base.html.twig` dans notre navigateur.


## Passer des variables

> - Pour **passer une variable** à un template on le passe sous la forme `['nomVariable' => $variable]`<br>
> - Pour utiliser la **variable** dans le **template twig**, on utilise `{{ nomVariable }}`

### Exemple

Fichier `HomeController.php` :

```php
public function homePage(): Response
{
    for($i = 0; $i < 10; $i++);
    return $this->render('base.html.twig', [
        'compteur' => $i,
    ]);
}
```

Fichier `base.html.twig` :

```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>{% block title %}Super titre pour mon site{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
    </head>
    <body>
        {% block body %}
        <div>
            {{ compteur }}
        </div>
        {% endblock %}

        {% block javascripts %} {% endblock %}
    </body>
</html>

```
?> Ici, on passe notre **variable compteur** dans notre template twig.

<br>

On peux evidemment envoyer d'autres formes de variables : 

```php
$stringSentence  = "On est fort en pomme";
$arrayBoissons = ['Coca', 'Orangina', 'Vin rouge', 'Sprite'];

return $this->render('base.html.twig', [
    'stringSentence' => $stringSentence,
    'arrayBoissons' => $arrayBoissons,
]);
```