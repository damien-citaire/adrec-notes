# Anatomie

## Construction d'une classe

!> Par convention, **un fichier = une classe**

- On créer notre notre **classe**,

- On définit ses **attributs** *($id, $name par exemple)*, il sont `private` <br>*(car dans l'exemple ci-dessous la classe ne sera pas `extend`)*,

- On met en place le **construct**, <br> *Le constructeur permet de construire une classe, il defini les paramètres qu'on peux lui passer.* <br> *Les paramètres nous permettent de passer/assigner des variables par exemple.*

- Ensuite c'est les **methodes** :
  - Pour chaque **attribut privé**, on crée un `getter` et un `setter` qui sont des **fonctions publiques** *(`public`)*,
  - On termine avec le `toString` pour gérer l'**affichage** de la classe, il retourne la classe sous forme de chaîne de caractère.<br>*(il n'est pas obligatoire mais si on fait un echo et qu'on n'as pas de toString, alors on n'as rien retour)*

#### Exemple :

```php
class Contact
{
    private int $id;
    private string $name;
    private string $mail;
    private int $age;

    /**
     * Contact constructor.
     */
    public function __construct()
    {
    }
    

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getMail(): string
    {
        return $this->mail;
    }
    /**
     * @param string $mail
     */
    public function setMail(string $mail): void
    {
        $this->mail = $mail;
    }

    /**
     * @return int
     */
    public function getAGe(): int
    {
        return $this->age;
    }
    /**
     * @param int $age
     */
    public function setAge(int $age): void
    {
        $this->age = $age;
    }


    public function __toString(): string
    {
        $contact = '</br>Nom du contact : ' . '<br>';
        $contact .= $this->name . ' (' . $this->mail . ')<br>';
        $contact .= '<i>' . $this->age . 'ans<i>';

        return $contact;
    }

}
```

#### Même Exemple *un peu plus lisible :*

![](img/phpobj_class-anatomie.png)

> Le `__construct` proposé ici est vide, ce qui est la même chose que de ne pas le préciser.

> Le mot-clé `this` représente notre classe, **celle dans laquelle on se situe**.

> Ici, le `__toString` utilise un `return`. On peux aussi mettre des `echo`, mais ça sert plutôt à afficher l'avancement/debuger pendant qu'on travaille dessus, en dehors de ça ils ne servent pas trop, le `return` sera + utilisé.


### Créer des classes sans forcer dans PhpStorm :muscle:

- **New Php class**, ça créer le fichier avec la classe,
- On prépare nos attributs,
- pour avoir le `__construct` automatiquement, on fait `⌘N` *(sur mac)* ou `Alt+Insert`*(sur windows)*, on selectionne **Constructor**, les attributs qu'on veux mettre dedans et tout est ok, magique, merveilleux :thumbsup: 
- pour avoir les **setter** et **getter** automatiquement, on fait `⌘N` *(sur mac)* ou `Alt+Insert`*(sur windows)*, on selectionne **Getter ans Setters**, les attributs pour lesquels on veut générer les getter/setter et tout est ok, magique, merveilleux :thumbsup:  :thumbsup: 

    Petit bonus *(valable aussi sur VS Code)*, `ctrl + space` pour les suggestions.

### Tips and tricks en plus

- **CTRL + clic gauche sur une méthode/classe** vous permet d'aller directement dessus,*changement de fichier si nécessaire*
- **CTRL + clic gauche sur une variable** vous permet d'aller sur la première définition de la variable
