# ADREC notes

Documentation generated with Docsify from notes taken during the Application Developer course in the 2020/2021 session.

### Preview Locally

```bash
$ cd my/awesome/doc/directory
$ docsify serve docs
```

By default, the process run on `http://localhost:3000`

Availlable online version [here](https://damien-citaire.gitlab.io/adrec-notes/#/)